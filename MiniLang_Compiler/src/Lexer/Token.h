//contain all different types of possible tokens

#ifndef TOKEN_H
#define TOKEN_H

enum class Token{
	
	Tok_AddOp,
	Tok_MultOp,
	Tok_TypeInt,
	Tok_TypeReal,
	Tok_Comment,
	Tok_BlockComment,
	Tok_Identifier,
	Tok_TypeString,
	Tok_RelOp,
	Tok_AssignmentOp,
	Tok_Punctuation,
	Tok_KW_real,
	Tok_KW_int,
	Tok_KW_bool,
	Tok_KW_string,
	Tok_TypeBool,
	Tok_KW_not,
	Tok_KW_return,
	Tok_KW_and,
	Tok_KW_or,
	Tok_KW_if,
	Tok_KW_else,
	Tok_KW_set,
	Tok_KW_var,
	Tok_KW_print,
	Tok_KW_while,
	Tok_KW_def,
	Tok_ERROR,
	Tok_EOF


};

#endif