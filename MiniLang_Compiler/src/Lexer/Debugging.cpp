#include "Debugging.h"

using namespace std;

//only for debugging
void Debugging::printToken(token t){
	cout << "______________" << '\n';
	printf("|Lexeme:%s|\n",t.lexeme.c_str());

	switch(t.tokenType){
		case Token::Tok_AddOp:
			cout<< "AddOP\n";
			break;
		case Token::Tok_MultOp:
			cout<< "MultOP\n";
			break;
		case Token::Tok_TypeInt:
			cout<< "TypeInt\n";
			break;
		case Token::Tok_TypeReal:
			cout<< "TypeReal\n";
			break;
		case Token::Tok_Comment:
			cout<< "Comment\n";
			break;
		case Token::Tok_BlockComment:
			cout<< "Block Comment\n";
			break;
		case Token::Tok_TypeString:
			cout<< "TypeString\n";
			break;	
		case Token::Tok_RelOp:
			cout<< "RelOp\n";
			break;
		case Token::Tok_AssignmentOp:
			cout<< "AssignmentOp\n";
			break;
		case Token::Tok_Punctuation:
			cout<< "Punctuation\n";
			break;
		case Token::Tok_KW_real:
			cout<< "Keyword Real\n";
			break;
		case Token::Tok_KW_int:
			cout<< "Keyword Int\n";
			break;
		case Token::Tok_KW_bool:
			cout<< "Keyword bool\n";
			break;
		case Token::Tok_KW_string:
			cout<< "Keyword string\n";
			break;
		case Token::Tok_TypeBool:
			cout<< "TypeBool\n";
			break;
		case Token::Tok_ERROR:
			cout<< "Error!\n";
			break;
		case Token::Tok_Identifier:
			cout<<"Identifier!\n";
			break;
		case Token::Tok_KW_not:
			cout<< "Keyword not\n";
			break;
		case Token::Tok_KW_return:
			cout<< "Keyword return\n";
			break;
		case Token::Tok_KW_and:
			cout<< "Keyword and\n";
			break;
		case Token::Tok_KW_or:
			cout<< "Keyword or\n";
			break;
		case Token::Tok_KW_if:
			cout<< "Keyword if\n";
			break;
		case Token::Tok_KW_else:
			cout<<"Keyword else\n";
			break;
		case Token::Tok_KW_set:
			cout<< "Keyword set\n";
			break;
		case Token::Tok_KW_var:
			cout<< "Keyword var\n";
			break;
		case Token::Tok_KW_print:
			cout<< "Keyword print\n";
			break;
		case Token::Tok_KW_while:
			cout<< "Keyword while\n";
			break;
		case Token::Tok_KW_def:
			cout<<"Keyword def\n";
			break;
		case Token::Tok_EOF:
			cout<<"EOF!\n";
			break;
		default:
			cout<< "Not matched";
	};

		cout << "------------" << '\n';
}

std::string Debugging::printTokenType(Token tokenType){
	switch(tokenType){
		case Token::Tok_AddOp:
			return "AddOP";
			
		case Token::Tok_MultOp:
			return "MultOP";
			
		case Token::Tok_TypeInt:
			return "TypeInt";
			
		case Token::Tok_TypeReal:
			return "TypeReal";
			
		case Token::Tok_Comment:
			return "Comment";
			
		case Token::Tok_BlockComment:
			return "Block Comment";
			
		case Token::Tok_TypeString:
			return "TypeString";
				
		case Token::Tok_RelOp:
			return "RelOp";
			
		case Token::Tok_AssignmentOp:
			return "AssignmentOp";
			
		case Token::Tok_Punctuation:
			return "Punctuation";
			
		case Token::Tok_KW_real:
			return "Real";
			
		case Token::Tok_KW_int:
			return "Int";
			
		case Token::Tok_KW_bool:
			return "Bool";
			
		case Token::Tok_KW_string:
			return "String";
			
		case Token::Tok_TypeBool:
			return "TypeBool";
			
		case Token::Tok_ERROR:
			return "Error!";
			
		case Token::Tok_Identifier:
			return"Identifier!";
			
		case Token::Tok_KW_not:
			return "KeywordNot";
			
		case Token::Tok_KW_return:
			return "KeywordReturn";
			
		case Token::Tok_KW_and:
			return "KeywordAnd";
			
		case Token::Tok_KW_or:
			return "KeywordOr";
			
		case Token::Tok_KW_if:
			return "KeywordIf";
			
		case Token::Tok_KW_else:
			return"KeywordElse";
			
		case Token::Tok_KW_set:
			return "KeywordSet";
			
		case Token::Tok_KW_var:
			return "KeywordVar";
			
		case Token::Tok_KW_print:
			return "KeywordPrint";
			
		case Token::Tok_KW_while:
			return "KeywordWhile";
			
		case Token::Tok_KW_def:
			return"KeywordDef";
			
		case Token::Tok_EOF:
			return"EOF!";
			
		default:
			return "Not matched";
	};
}

void Debugging::printStack(stack<int> myStack){
	for (stack<int> dump = myStack; !dump.empty(); dump.pop())
        cout << dump.top() << '\n';

    cout << "(" << myStack.size() << " elements)\n";
}

void Debugging::printVectorTokens(vector<token> v){
	for (vector<token>::iterator it = v.begin() ; it != v.end(); ++it){
		printToken(*it);
	}
}