#ifndef DEBUGGING_H
#define DEBUGGING_H

#include "Token.h"
#include "Lexer.h"
#include <stack>
#include <iostream>
#include <string>

class Debugging{

	public:
		void printToken(token t);
		std::string printTokenType(Token tokenType);
		void printStack(stack<int> myStack);
		void printVectorTokens(vector<token> v);

};

#endif