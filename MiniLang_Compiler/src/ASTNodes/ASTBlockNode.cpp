#include "ASTBlockNode.h"

ASTBlockNode::ASTBlockNode(){};

void ASTBlockNode::addStatement(ASTStmntNode* statement){
	statements.push_back(statement);
}


ASTBlockNode::~ASTBlockNode(){
	while(statements.size()!=0){
		statements.pop_back();
	}
}

std::vector <ASTStmntNode*> ASTBlockNode::getStatements(){
	return statements;
}