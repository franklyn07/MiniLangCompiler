#include "ASTFuncDeclNode.h"

ASTFuncDeclNode::ASTFuncDeclNode(ASTIdentifierNode* identifier, ASTFormalParamsNode* formalParams, std::string tokenType, ASTBlockNode* block){
	this -> identifier = identifier;
	this -> formalParams = formalParams;
	this -> tokenType = tokenType;
	this -> block = block;
}

ASTFuncDeclNode::~ASTFuncDeclNode(){
	delete formalParams;
	delete identifier;
	delete block;
}

ASTIdentifierNode* ASTFuncDeclNode::getIdentifier(){
	return identifier;
}

ASTFormalParamsNode* ASTFuncDeclNode::getFormalParams(){
	return formalParams;
}

std::string ASTFuncDeclNode::getTokenType(){
	return tokenType;
}

ASTBlockNode* ASTFuncDeclNode::getBlock(){
	return block;
}