#include "ASTFormalParamNode.h"

ASTFormalParamNode::ASTFormalParamNode(ASTIdentifierNode* identifier, std::string tokenType){
	this -> identifier = identifier;
	this -> tokenType = tokenType;
}

ASTIdentifierNode* ASTFormalParamNode::getIdentifier(){
	return identifier;
}

std::string ASTFormalParamNode::getTokenType(){
	return tokenType;
}