#ifndef ASTIFNODE_H
#define ASTIFNODE_H

#include "ASTStmntNode.h"
#include "ASTBlockNode.h"
#include "ASTExprNode.h"

class ASTIfNode: public ASTStmntNode{
	public:

		//constructors
		ASTIfNode(ASTExprNode* expression,ASTBlockNode* blockL);
		ASTIfNode(ASTExprNode* expression,ASTBlockNode* blockL, ASTBlockNode* blockR);

		//destructor
		~ASTIfNode();

		//getters
		ASTExprNode* getExpression();
		ASTBlockNode* getLeftBlock();
		ASTBlockNode* getRightBlock();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//method to be destroyed
		void dummy () override {};

		//private fields to hold pointers
		ASTExprNode* expression;
		//if block
		ASTBlockNode* blockL;
		//else block
		ASTBlockNode* blockR;
};

#endif