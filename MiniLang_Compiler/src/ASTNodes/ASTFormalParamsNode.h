#ifndef ASTFORMALPARAMSNODE_H
#define ASTFORMALPARAMSNODE_H

#include "ASTStmntNode.h"
#include "ASTFormalParamNode.h"
#include <vector>

class ASTFormalParamsNode: public ASTStmntNode{
	public:

		//constructor
		ASTFormalParamsNode(){};

		//destructor
		~ASTFormalParamsNode();

		//method to add formalparam nodes
		void addFormalParam(ASTFormalParamNode* formalParam);

		//getters
		std::vector<ASTFormalParamNode*> getVector();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};

	private:
		//vector that holds nodes of type formalParam
		std::vector<ASTFormalParamNode*> formalParamNodes;

		//to be destroyed
		void dummy() override {};

};

#endif