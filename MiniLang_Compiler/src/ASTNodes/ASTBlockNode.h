#ifndef ASTBLOCKNODE_H
#define ASTBLOCKNODE_H

#include "ASTStmntNode.h"
#include <vector>

class ASTBlockNode: public ASTStmntNode{
	public:

		// constructors
		ASTBlockNode ();

		//method to add statement pointers
		void addStatement(ASTStmntNode* statement);

		//destructor
		~ASTBlockNode();

		//getters
		std::vector <ASTStmntNode*> getStatements();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};

	private:
		//pointer to statement node
		std::vector <ASTStmntNode*> statements;

		//delete method
		void dummy () override{};
};

#endif