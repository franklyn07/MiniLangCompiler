#include "ASTBooleanLiteralNode.h"

ASTBooleanLiteralNode::ASTBooleanLiteralNode(bool booleanValue){
	this->booleanValue = booleanValue;
}

bool ASTBooleanLiteralNode::getBooleanValue(){
	return booleanValue;
}

/*void ASTNumberExprNode::accept(Visitor * visitor){
	visitor->visit(this);
}*/