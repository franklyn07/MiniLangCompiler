#ifndef ASTNODE_H
#define ASTNODE_H

#include "../Visitors/Visitor.h"

class ASTNode{
	public:
		//accept visitor method
		virtual void accept(Visitor* v)=0;

	private:
		//virtual method to make node virtual
		virtual void dummy()=0;
};

#endif