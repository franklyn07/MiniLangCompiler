#include "ASTIntegerLiteralNode.h"

ASTIntegerLiteralNode::ASTIntegerLiteralNode(int integerValue){
	this->integerValue = integerValue;
}

/*void ASTNumberExprNode::accept(Visitor * visitor){
	visitor->visit(this);
}*/

int ASTIntegerLiteralNode::getInteger(){
	return integerValue;
}