#ifndef ASTFUNCTIONCALLNODE_H
#define ASTFUNCTIONCALLNODE_H

#include "./ASTExprNode.h"
#include "./ASTIdentifierNode.h"

class ASTFunctionCallNode:public ASTExprNode{
	
	public:

		//multiple kinds of constructors
		ASTFunctionCallNode(ASTIdentifierNode* identifier);
		ASTFunctionCallNode(ASTIdentifierNode* identifier, ASTExprNode* firstExpr);
		ASTFunctionCallNode(ASTIdentifierNode* identifier, ASTExprNode* firstExpr, ASTExprNode* secondExpr);

		~ASTFunctionCallNode();

		//getters
		ASTIdentifierNode* getIdentifier();
		ASTExprNode* getFirstExprNode();
		ASTExprNode* getSecondExprNode();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};

		ASTIdentifierNode* identifier;
		ASTExprNode* firstExpr;
		ASTExprNode* secondExpr;

};

#endif