#include "ASTPrintNode.h"

ASTPrintNode::ASTPrintNode (ASTExprNode* expression){
	this -> expression = expression;
}

ASTPrintNode::~ASTPrintNode(){
	delete expression;
}

ASTExprNode* ASTPrintNode::getExpression(){
	return expression;
}