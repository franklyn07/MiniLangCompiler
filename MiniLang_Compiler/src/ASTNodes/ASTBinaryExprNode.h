#ifndef ASTBINARYEXPRNODE_H
#define ASTBINARYEXPRNODE_H

#include "./ASTExprNode.h"
#include <string>

class ASTBinaryExprNode : public ASTExprNode{
	public:

		//constructor
		ASTBinaryExprNode(std::string Operator, ASTExprNode * LHS, ASTExprNode * RHS);

		//destructor
		virtual ~ASTBinaryExprNode();

		//getters
		std::string getOperator();
		ASTExprNode* getLeftExpression();
		ASTExprNode* getRightExpression();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};

	private:

		//needs to be cleared done to override dummy
		void dummy()override{}

		std::string Operator;
		ASTExprNode* LHS;
		ASTExprNode* RHS;
};

#endif