#include "ASTRealLiteralNode.h"

ASTRealLiteralNode::ASTRealLiteralNode(float realValue){
	this->realValue = realValue;
}

/*void ASTNumberExprNode::accept(Visitor * visitor){
	visitor->visit(this);
}*/

float ASTRealLiteralNode::getRealValue(){
	return realValue;
}