#ifndef ASTINTEGERLITERALNODE_H
#define ASTINTEGERLITERALNODE_H

#include "./ASTExprNode.h"

class ASTIntegerLiteralNode: public ASTExprNode{
	
	public:

		//constructor
		ASTIntegerLiteralNode(int integerValue);
		~ASTIntegerLiteralNode(){};

		//getters
		int getInteger();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};
		
		int integerValue;
};

#endif