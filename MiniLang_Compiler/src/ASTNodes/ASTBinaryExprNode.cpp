#include "ASTBinaryExprNode.h"

ASTBinaryExprNode::ASTBinaryExprNode(std::string Operator, ASTExprNode * LHS, ASTExprNode * RHS){
	this->Operator = Operator;
	this->LHS = LHS;
	this->RHS = RHS;	
}

ASTBinaryExprNode::~ASTBinaryExprNode(){
	delete LHS;
	delete RHS;
}

std::string ASTBinaryExprNode::getOperator(){
	return Operator;
}

ASTExprNode* ASTBinaryExprNode::getLeftExpression(){
	return LHS;
}

ASTExprNode* ASTBinaryExprNode::getRightExpression(){
	return RHS;
}

/*void ASTBinaryExprNode::accept(Visitor * visitor){
	visitor -> visit(this);
}*/