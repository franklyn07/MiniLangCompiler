#include "ASTVariableDeclarationNode.h"
#include <iostream>

ASTVariableDeclarationNode::ASTVariableDeclarationNode(ASTIdentifierNode* identifier, std::string type, ASTExprNode* exprNodePointer){
	this->identifier = identifier;
	this->exprNodePointer = exprNodePointer;
	this->type = type;
}

ASTVariableDeclarationNode::~ASTVariableDeclarationNode(){
	delete identifier;
	delete exprNodePointer;
}

ASTIdentifierNode* ASTVariableDeclarationNode::getIdentifier(){
	return identifier;
}

std::string ASTVariableDeclarationNode::getTokenType(){
	return type;
}

ASTExprNode* ASTVariableDeclarationNode::getExpression(){
	return exprNodePointer;
}