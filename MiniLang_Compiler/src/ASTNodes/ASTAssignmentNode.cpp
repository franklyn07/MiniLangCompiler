#include "ASTAssignmentNode.h"
#include <iostream>

ASTAssignmentNode::ASTAssignmentNode(ASTIdentifierNode* identifier, ASTExprNode* exprNodePointer){
	this->identifier = identifier;
	this->expression = exprNodePointer;
}

ASTAssignmentNode::~ASTAssignmentNode(){
	delete identifier;
	delete expression;
}

ASTIdentifierNode* ASTAssignmentNode::getIdentifier(){
	return identifier;
}

ASTExprNode* ASTAssignmentNode::getExpression(){
	return expression;
}
