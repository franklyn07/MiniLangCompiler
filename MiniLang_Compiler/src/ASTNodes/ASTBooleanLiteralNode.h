#ifndef ASTBOOLEANLITERALNODE_H
#define ASTBOOLEANLITERALNODE_H

#include "./ASTExprNode.h"

class ASTBooleanLiteralNode: public ASTExprNode{
	
	public:

		//constructor
		ASTBooleanLiteralNode(bool booleanValue);
		~ASTBooleanLiteralNode(){};

		//getters
		bool getBooleanValue();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};

		bool booleanValue;
};

#endif