#ifndef ASTIDENTIFIERNODE_H
#define ASTIDENTIFIERNODE_H

#include "./ASTExprNode.h"
#include <string>

class ASTIdentifierNode: public ASTExprNode{
	
	public:

		//constructor
		ASTIdentifierNode(std::string identifier);
		~ASTIdentifierNode(){};

		//getters
		std::string getIdentifier();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};
		
		//will store the identifier
		std::string identifier;
};

#endif