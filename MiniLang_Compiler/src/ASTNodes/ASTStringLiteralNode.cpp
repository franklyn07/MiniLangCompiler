#include "ASTStringLiteralNode.h"

ASTStringLiteralNode::ASTStringLiteralNode(std::string stringValue){
	this->stringValue = stringValue;
}

/*void ASTNumberExprNode::accept(Visitor * visitor){
	visitor->visit(this);
}*/

std::string ASTStringLiteralNode::getString(){
	return stringValue;
}