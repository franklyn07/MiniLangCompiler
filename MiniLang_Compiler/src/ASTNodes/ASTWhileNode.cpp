#include "ASTWhileNode.h"

ASTWhileNode::ASTWhileNode(ASTExprNode* expression, ASTBlockNode* block){
	this->block = block;
	this->expression = expression;
}

ASTWhileNode::~ASTWhileNode(){
	delete block;
	delete expression;
}

ASTExprNode* ASTWhileNode::getExpression(){
	return expression;
}

ASTBlockNode* ASTWhileNode::getBlock(){
	return block;
}