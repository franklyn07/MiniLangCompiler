#ifndef ASTEXPRNODE_H
#define ASTEXPRNODE_H

#include "./ASTNode.h"

class ASTExprNode: public ASTNode{
	public:
		
		virtual ~ASTExprNode(){};

	private:
		//dummy method is inherited to make it a pure virtual class
};

#endif