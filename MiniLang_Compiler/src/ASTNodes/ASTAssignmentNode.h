#ifndef ASTASSIGNMENTNODE_H
#define ASTASSIGNMENTNODE_H

#include "ASTStmntNode.h"
#include "ASTExprNode.h"
#include "ASTIdentifierNode.h"

class ASTAssignmentNode:public ASTStmntNode{
	public:

		//constructor
		ASTAssignmentNode(ASTIdentifierNode* identifier, ASTExprNode * expression);

		~ASTAssignmentNode();

		//getters
		ASTIdentifierNode* getIdentifier();
		ASTExprNode* getExpression();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed

		void dummy()override{};

		ASTIdentifierNode* identifier;
		ASTExprNode * expression;

};

#endif