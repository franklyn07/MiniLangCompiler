//used as testing before writing repl

#include "./Lexer/Lexer.h"
#include "./Lexer/Debugging.h"
#include "./Parser/Parser.h"
#include "./Visitors/VisitorXML.h"
#include "./Visitors/VisitorSemAn.h"
#include "./Visitors/VisitorInterpreter.h"

int main (int argc, char*argv[]){
	
	//tokenize given program
	Lexer* lexer = new Lexer();

	lexer->tokenizeProgram(argv[1]);

	Parser* parser = new Parser(lexer);

	parser->parse();

	VisitorXML* xmlWriter = new VisitorXML();

	xmlWriter->writeXML(parser->getASTStatementNodes());

	//performs semantic analysis upon ast
	
	VisitorSemAn* semanticAnalysis = new VisitorSemAn();
	
	semanticAnalysis->analyze(parser->getASTStatementNodes());

	VisitorInterpreter* interpreter = new VisitorInterpreter();

	interpreter->interpret(parser->getASTStatementNodes());
	
	/*//parse it
	Parser parser(lexer);

	//produce xml file
	VisitorXML xmlWriter;

	//performs semantic analysis upon ast
	VisitorSemAn semanticAnalysis;

	//interpret code
	VisitorInterpreter interpeter;

	if(lexer->tokenizeProgram(prog)){
		if(parser->parse()){
			xmlWriter->writeXML(parser->getASTStatementNodes());
			if(semanticAnalysis->analyze(parser->getASTStatementNodes()))
				if(interpreter->interpret(parser->getASTStatementNodes()))
		}
	}

	//cleanup
	lexer->cleanVectorOfTokens();
	parser->clearASTStatementNodes();
	semanticAnalysis->clearScopeAnalyzer();
	interpreter->clearScopeInterpreter();*/

} 