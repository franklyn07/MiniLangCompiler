//Interactive mode for interpreter

#include "./Lexer/Lexer.h"
#include "./Parser/Parser.h"
#include "./Visitors/VisitorXML.h"
#include "./Visitors/VisitorSemAn.h"
#include "./Visitors/VisitorInterpreter.h"
#include <string>
#include <stack>
#include <iostream>
#include <fstream>
#include <list>

using namespace std;

//global variables
//holds whether loop should keep on running
bool run = true;

//holds whether a program is already loaded in scope
bool loadedProgram = false;

//holds local scopes
stack<interpreterScope> localScope;

//holds command
string command;

//holds parsed command
string parsedCommand;

//holds parameter if any
string parameter;

//initialise lexer,parser, semantic analyzer and interpeter
//tokenize given program
Lexer* lexer = new Lexer();

//parse it
Parser* parser = new Parser(lexer);

//produce xml file
VisitorXML* xmlWriter = new VisitorXML();

//performs semantic analysis upon ast
VisitorSemAn* semanticAnalysis = new VisitorSemAn();

//interpret code
VisitorInterpreter* interpreter = new VisitorInterpreter();

//function prototypes

/*//updates special variable which holds answer of last function
void updateSpecialVar(string newAns);*/

//take input function and write it to a text file
void writeToTextFile();

//splits standard command
void parseStandardCommand();

//prints out commands
void help();

//loads the program scope of the given program into local scope
void load(string prog);

//prints all the ids and their values in scope 
void st();

//interprets given program
void interpret(string prog);

//empty scopes and start afresh
void stop();

//cleanes all tokens, astnodes,symboltable, and interpreter table
void cleaner();

//tries to go through the process of tokenizing parsing, etc.
//if it fails it cleans up after itself
bool execute(string prog);

//write to already existing program
void writeToProgram(string loadedProg, string command);

//quit minilangi
void quit();

//main function
int main(){

	//print commands
	help();

	while(run){
		cout<<"\n>>";
		
		//get command
		getline(cin,command);

		//check if first character is special character
		if(command.at(0) == '#'){
			//if yes parse command
			parseStandardCommand();

			//check command we got
			if(parsedCommand.compare("help")==0){
				help();
			}else if(parsedCommand.compare("load")==0){
				if(!loadedProgram){
					load(parameter);
				}else{
					cout << "Program Already Loaded! One Program at a time!"<<endl;
				}
			}else if(parsedCommand.compare("st")==0){
				st();
			}else if(parsedCommand.compare("interpret")==0){
				interpret(parameter);
			}else if(parsedCommand.compare("stop")==0){
				stop();
			}else if(parsedCommand.compare("quit")==0){
				quit();
			}
		}else{
			//if no program is loaded write outside
			if(!loadedProgram){
				//else write given command to text file 
				writeToTextFile();

				//execute file
				execute("./temp.txt");

				//clear input tokens
				lexer->cleanVectorOfTokens();
				parser->clearASTStatementNodes();
			}else{
				//write in it
				writeToProgram(parameter, command);
				cleaner();
				execute(parameter);
			}
		}
	}

	//clean everything
	cleaner();
	return 0; 
}

//function definitions
void parseStandardCommand(){
	//will hold commands and parameter
	string stdCommand = "";
	string locParameter = "";

	//flag to check if space is met
	bool flag = false;

	//iterate through whole command - skip first char due to special symbol
	for(int i = 1; i<command.length(); i++){
		//current char
		char currentChar = command[i];
		//check if space
		if ((int)currentChar==32){
			flag = true;
		}else if(flag == false){
			stdCommand.push_back(currentChar);
		}else if(flag == true){
			locParameter.push_back(currentChar);
		}
	}

	//set parsed commands to the global commands if changed
	if(stdCommand.length()>0){
		parsedCommand = stdCommand;
	}

	if(locParameter.length()>0){
		parameter = locParameter;
	}
}

//prints out commands
void help(){
	cout<<"\nCommands: \n #help -- shows commands\n #load <program address> -- load program\n #st -- shows the current variables\n #interpret <programm addres>-- interprets given program\n #stop -- stops current program and you can load a new one\n #quit -- exits interactive mode\n Any other command will be interpreted accordingly!\n Note: Functions should be written in one line!"<<endl;
}

//loads the program scope of the given program into local scope
void load(string prog){
	if(execute(prog)){
		//set boolean true
		loadedProgram = true;
	}
	
}

//prints all the ids and their values in scope 
void st(){
	interpreter->interTable->printTopScope();
}

//interprets given program
void interpret(string prog){
	execute(prog);	

	//cleanup after interpretation
	cleaner();
}

//empty scopes and start afresh
void stop(){
	cleaner();
	//set loaded program to false
	loadedProgram = false;
}

//quit minilangi
void quit(){
	run = false;
}

//cleanes all tokens, astnodes,symboltable, and interpreter table
void cleaner(){
	lexer->cleanVectorOfTokens();
	parser->clearASTStatementNodes();
	semanticAnalysis->clearScopeAnalyzer();
	interpreter->clearScopeInterpreter();
}

bool execute(string prog){
	//if any of the steps fails - stop and do clean up
	if(lexer->tokenizeProgram(prog)){
		if(parser->parse()){
			xmlWriter->writeXML(parser->getASTStatementNodes());
			if(semanticAnalysis->analyze(parser->getASTStatementNodes())){
				if(interpreter->interpret(parser->getASTStatementNodes())){
					return true;
				}else{
					lexer->cleanVectorOfTokens();
					parser->clearASTStatementNodes();
					semanticAnalysis->clearScopeAnalyzer();
					interpreter->clearScopeInterpreter();
					return false;
				}
			}else{
				lexer->cleanVectorOfTokens();
				parser->clearASTStatementNodes();
				semanticAnalysis->clearScopeAnalyzer();
				return false;
			}
		}else{
			lexer->cleanVectorOfTokens();
			parser->clearASTStatementNodes();
			return false; 
		}
	}else{
		lexer->cleanVectorOfTokens();
		return false;
	}
}

//write given output to file
void writeToTextFile(){
	//open a temporary file
	ofstream commandFile("temp.txt");
	//write to it command and close file
	if (commandFile.is_open()){
		commandFile << command;
		commandFile.close();
	}else {
		cout << "Unable to open file";
	}
}

//write to loaded program
void writeToProgram(string loadedProg, string command){
	ofstream loadedFile;

	//open loadedfile
	loadedFile.open(loadedProg,ios_base::app);
	loadedFile<<command<<"\n";
	loadedFile.close();
}