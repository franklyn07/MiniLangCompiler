#ifndef VISITOR_H
#define VISITOR_H

//forward delcaration of ASTNode to break circular dependency between node and visitor
class ASTNode;
class ASTStmntNode;
class ASTExprNode;
class ASTPrintNode;
class ASTRealLiteralNode;
class ASTReturnNode;
class ASTAssignmentNode;
class ASTFormalParamNode;
class ASTFormalParamsNode;
class ASTFuncDeclNode;
class ASTFunctionCallNode;
class ASTIdentifierNode;
class ASTIfNode;
class ASTIntegerLiteralNode;
class ASTStringLiteralNode;
class ASTUnaryNode;
class ASTVariableDeclarationNode;
class ASTWhileNode;
class ASTAssignmentNode;
class ASTBinaryExprNode;
class ASTBlockNode;
class ASTBooleanLiteralNode;

class Visitor{
	public:
		//visit method for all concrete astnode classes
		//note it is purely virtual since this is a purely virtual class
		virtual void visit(ASTPrintNode* node)=0;
		virtual void visit(ASTRealLiteralNode* node)=0;
		virtual void visit(ASTReturnNode* node)=0;
		virtual void visit(ASTAssignmentNode* node)=0;
		virtual void visit(ASTFormalParamNode* node)=0;
		virtual void visit(ASTFormalParamsNode* node)=0;
		virtual void visit(ASTFuncDeclNode* node)=0;
		virtual void visit(ASTFunctionCallNode* node)=0;
		virtual void visit(ASTIdentifierNode* node)=0;
		virtual void visit(ASTIfNode* node)=0;
		virtual void visit(ASTIntegerLiteralNode* node)=0;
		virtual void visit(ASTStringLiteralNode* node)=0;
		virtual void visit(ASTUnaryNode* node)=0;
		virtual void visit(ASTVariableDeclarationNode* node)=0;
		virtual void visit(ASTWhileNode* node)=0;
		virtual void visit(ASTBinaryExprNode* node)=0;
		virtual void visit(ASTBlockNode* node)=0;
		virtual void visit(ASTBooleanLiteralNode* node)=0;
};

#endif