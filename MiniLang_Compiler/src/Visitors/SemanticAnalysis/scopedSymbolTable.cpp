//Used by VisitorSemAn.h

#include "./scopedSymbolTable.h"

scopedSymbolTable::scopedSymbolTable(){
	//initialise a global scope
	struct Scope global;

	//add global scope to stack of scopes
	pushNewScope(global);
}


//push new scope on stack
void scopedSymbolTable::pushNewScope(Scope s){
	symbolTable.push(s);
}

//pop the top scope from stack
void scopedSymbolTable::popTopScope(){
	symbolTable.pop();
}

//insert a type 
void scopedSymbolTable::insertIdandType_CurrentScope(std::string id,Type* t){
	symbolTable.top().scope.insert({id,t});
}

//find item in map 
// if not found returns iterator to end, else returns iterator
Type* scopedSymbolTable::findEntity(std::string identifier){
	//will hold the scopes temporarily
	std::stack<Scope> temp;

	//will hold iterator of map
	std::unordered_map<std::string,Type*>::const_iterator it;

	while(!symbolTable.empty()){

		//try to find the identifier in the current scope
		it = symbolTable.top().scope.find(identifier);

		//if identifier is found
		if (it != symbolTable.top().scope.end()){
			//check whether temp is empty - if not put all scopes back in
			if(!temp.empty()){
				while(!temp.empty()){
					symbolTable.push(temp.top());
					temp.pop();
				}
			}

			//return Type object
			return it->second;
		}else{
			//if not found
			//pop top scope and store it temporarily in temp
			temp.push(symbolTable.top());
			symbolTable.pop();
		}
	}

	//repopulating symbol table if id is never found
	if(!temp.empty()){
		while(!temp.empty()){
			symbolTable.push(temp.top());
			temp.pop();
		}
	}

	Type* t = new Type("NULL");
	//if nothing is returned up till now return null
	return t;
}

Type* scopedSymbolTable::findInScope(std::string identifier){
//will hold iterator of map
	std::unordered_map<std::string,Type*>::const_iterator it;

	it = symbolTable.top().scope.find(identifier);

	//if identifier is found
	if (it != symbolTable.top().scope.end()){
		//return Type object
		return it->second;
	}else{
		Type* t = new Type("NULL");
		return t;
	}

}

//cleanup
void scopedSymbolTable::clearScope(){
	while(!symbolTable.empty()){
		symbolTable.top().scope.clear();
		symbolTable.pop();
	}

	//initialise a global scope
	struct Scope global;

	//add global scope to stack of scopes
	pushNewScope(global);
}

//print top scope of symbol table
void scopedSymbolTable::printTopScope(){
	for(auto current:symbolTable.top().scope){
		std::cout<<current.first<<std::endl;
	}
}