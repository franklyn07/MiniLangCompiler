//this class will hold the types of the identifiers

#ifndef TYPE_H
#define TYPE_H

#include <string>
#include <vector>
#include <algorithm>

class Type{
	public:
		//constructor for when we only have one type
		//mainly all the times except for when we have //a function
		Type(std::string myType);

		//constructor for when we have both a type as 
		//well as parameter types -- in functions
		Type(std::string myType, std::vector<std::string> myParamTypes);

		//destructor
		~Type(){};

		//if type has same token type as current id return true
		bool compareTokenType(std::string identifierType);
		bool compareParams(std::vector <std::string> parameterTypes);

		//holders of my types
		std::string myType;
		std::vector <std::string> myParamTypes;
};

#endif