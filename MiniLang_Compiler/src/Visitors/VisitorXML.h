#ifndef VISITOR_XML_H
#define VISITOR_XML_H

#include "Visitor.h"
#include "../Lexer/Debugging.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class VisitorXML:public Visitor{
	public:
		//constructor to open file which we will be writing in
		VisitorXML(){};

		//destructor
		~VisitorXML(){};

		//produces xml file
		void writeXML(std::vector<ASTStmntNode*> statements);


		//ovverriding visit methods according to astnode type
		void visit(ASTPrintNode* node) override;
		void visit(ASTRealLiteralNode* node)override;
		void visit(ASTReturnNode* node)override;
		void visit(ASTAssignmentNode* node)override;
		void visit(ASTFormalParamNode* node)override;
		void visit(ASTFormalParamsNode* node)override;
		void visit(ASTFuncDeclNode* node)override;
		void visit(ASTFunctionCallNode* node)override;
		void visit(ASTIdentifierNode* node)override;
		void visit(ASTIfNode* node)override;
		void visit(ASTIntegerLiteralNode* node)override;
		void visit(ASTStringLiteralNode* node)override;
		void visit(ASTUnaryNode* node)override;
		void visit(ASTVariableDeclarationNode* node)override;
		void visit(ASTWhileNode* node)override;
		void visit(ASTBinaryExprNode* node)override;
		void visit(ASTBlockNode* node)override;
		void visit(ASTBooleanLiteralNode* node)override;

	private:
		//keeps count of the number of tabs to be printed
		int tabCount;

		//writes the tabs to the file
		void printTabs();

		//writes error message
		void printError();

		//pointer checker
		bool checkPointer(ASTNode* node);

		//used to print token types in xml file
		Debugging d;

		//used to write to file
		ofstream astFile;

};

#endif