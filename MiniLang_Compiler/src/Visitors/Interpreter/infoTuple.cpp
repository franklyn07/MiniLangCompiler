//this class will hold the info of each id for interpreter

#include "infoTuple.h"

//constructor for when we only know the value of the id
//mainly happens when error occurred
infoTuple::infoTuple(std::string value){
	this->value = value;
	type = Token::Tok_ERROR;
	parameters = nullptr;
	block = nullptr;
}

//constructor for when we only know the type, and we dont know the value
//used in formalparams mainly
infoTuple::infoTuple(Token type){
	value = "";
	this->type = type;
	parameters = nullptr;
	block = nullptr;
}


//constructor for when we know both the id and the token type
infoTuple::infoTuple(std::string value, Token type){
	this->value = value;
	this->type = type;
	parameters = nullptr;
	block = nullptr;
}

//constructor for when we have a function thus we know the
//block of execution and the formal paramaters node
infoTuple::infoTuple(ASTFormalParamsNode* parameters, ASTBlockNode* block){
	value = "";
	type = Token::Tok_ERROR;
	this->parameters = parameters;
	this->block = block;
}

//destructor
infoTuple::~infoTuple(){
}

//needed setters
void infoTuple::setValue(std::string value){
	this->value = value;
}

//getters
std::string infoTuple::getValue(){
	return value;
}

Token infoTuple::getTokenType(){
	return type;
}

ASTFormalParamsNode* infoTuple::getParameters(){
	return parameters;
}

ASTBlockNode* infoTuple::getBlock(){
	return block;
}