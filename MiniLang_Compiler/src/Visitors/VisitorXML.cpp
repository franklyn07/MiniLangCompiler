#include "VisitorXML.h"
#include "../ASTNodes/ASTNode.h"
#include "../ASTNodes/ASTStmntNode.h"
#include "../ASTNodes/ASTExprNode.h"
#include "../ASTNodes/ASTVariableDeclarationNode.h"
#include "../ASTNodes/ASTAssignmentNode.h"
#include "../ASTNodes/ASTPrintNode.h"
#include "../ASTNodes/ASTIfNode.h"
#include "../ASTNodes/ASTWhileNode.h"
#include "../ASTNodes/ASTReturnNode.h"
#include "../ASTNodes/ASTFuncDeclNode.h"
#include "../ASTNodes/ASTBlockNode.h"
#include "../ASTNodes/ASTBinaryExprNode.h"
#include "../ASTNodes/ASTBooleanLiteralNode.h"
#include "../ASTNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ASTIdentifierNode.h"
#include "../ASTNodes/ASTRealLiteralNode.h"
#include "../ASTNodes/ASTIntegerLiteralNode.h"
#include "../ASTNodes/ASTStringLiteralNode.h"
#include "../ASTNodes/ASTUnaryNode.h"
#include "../ASTNodes/ASTFormalParamNode.h"
#include "../ASTNodes/ASTFormalParamsNode.h"


void VisitorXML::visit(ASTPrintNode* node) {
	printTabs();
	astFile << "<PrintExpr>\n";
	tabCount+=1;
	if(checkPointer(node->getExpression())){
		node -> getExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile << "</PrintExpr>\n";
}

void VisitorXML::visit(ASTRealLiteralNode* node){
	printTabs();
	astFile << "<Real>"+std::to_string(node->getRealValue())+"</Real>\n";
}

void VisitorXML::visit(ASTReturnNode* node){
	printTabs();
	astFile<<"<Return>\n";
	tabCount+=1;
	if(checkPointer(node->getExpression())){
		node -> getExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile<<"</Return>\n";
}

void VisitorXML::visit(ASTAssignmentNode* node){
	printTabs();
	astFile << "<Assignment>\n";
	tabCount+=1;
	if(checkPointer(node->getIdentifier())){
		node -> getIdentifier()->accept(this);
	}
	if(checkPointer(node->getExpression())){
		node -> getExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile<<"</Assignment>\n";
}

void VisitorXML::visit(ASTFormalParamNode* node){
	printTabs();
	astFile<<"<FormalParam>\n";
	tabCount+=1;

	if(checkPointer(node->getIdentifier())){
		node -> getIdentifier()->accept(this);
	}

	printTabs();
	astFile<<"<Type>"+ node->getTokenType()+"</Type>\n";
	tabCount-=1;
	printTabs();
	astFile<<"</FormalParam>\n";
}

void VisitorXML::visit(ASTFormalParamsNode* node){
	printTabs();
	astFile<<"<FormalParams>\n";
	tabCount+=1;
	std::vector<ASTFormalParamNode*> localVec = node -> getVector();
	for (ASTFormalParamNode* currentPtr : localVec){
		if(checkPointer(currentPtr)){
			currentPtr->accept(this);
		}
	}
	tabCount-=1;
	printTabs();
	astFile << "</FormalParams>\n";
}

void VisitorXML::visit(ASTFuncDeclNode* node){
	printTabs();
	astFile<<"<FunctionDecl>\n";
	tabCount+=1;

	if(checkPointer(node->getIdentifier())){
		node -> getIdentifier()->accept(this);
	}

	printTabs();
	astFile<<"<Type>"+node->getTokenType()+"</Type>\n";

	if(checkPointer(node -> getFormalParams())){
		node -> getFormalParams()->accept(this);
	}
	if(checkPointer(node -> getBlock())){
		node -> getBlock()->accept(this);
	}

	tabCount-=1;
	printTabs();
	astFile << "</FunctionDecl>\n";
}

void VisitorXML::visit(ASTFunctionCallNode* node){
	printTabs();
	astFile<<"<FunctionCall>\n";
	tabCount+=1;
	printTabs();

	if(checkPointer(node -> getIdentifier())){
		node -> getIdentifier()->accept(this);
	}

	if(checkPointer(node -> getFirstExprNode())){
		node -> getFirstExprNode()->accept(this);
	}

	if(checkPointer(node -> getSecondExprNode())){
		node -> getSecondExprNode()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile<<"</FunctionCall>\n";
}

void VisitorXML::visit(ASTIdentifierNode* node){
	printTabs();
	astFile<<"<Identifier>"+node->getIdentifier()+"</Identifier>\n";
}

void VisitorXML::visit(ASTIfNode* node){
	printTabs();
	astFile<<"<IfNode>\n";
	tabCount+=1;

	if(checkPointer(node -> getExpression())){
		node -> getExpression()->accept(this);
	}

	if(checkPointer(node -> getLeftBlock())){
		node -> getLeftBlock()->accept(this);
	}

	if(checkPointer(node -> getRightBlock())){
		node -> getRightBlock()->accept(this);
	}

	tabCount-=1;
	printTabs();
	astFile<<"</IfNode>\n";
}

void VisitorXML::visit(ASTIntegerLiteralNode* node){
	printTabs();
	astFile<<"<IntegerLiteral>"+std::to_string(node->getInteger())+"</IntegerLiteral>\n";
}

void VisitorXML::visit(ASTStringLiteralNode* node){
	printTabs();
	astFile<<"<StringLiteral>"+node->getString()+"</StringLiteral>\n";
}

void VisitorXML::visit(ASTUnaryNode* node){
	printTabs();
	astFile << "<Unary Type=\""+node->getString()+"\">\n";
	tabCount+=1;
	if(checkPointer(node -> getExpression())){
		node -> getExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile<<"</Unary>\n";
}


void VisitorXML::visit(ASTVariableDeclarationNode* node){
	printTabs();
	astFile << "<VarDecl>\n";
	tabCount+=1;
	printTabs();

	if(checkPointer(node->getIdentifier())){
		node->getIdentifier()->accept(this);
	}

	astFile << "<Type>"+ node->getTokenType()+"</Type>\n";
	
	if(checkPointer(node->getExpression())){
		node->getExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile << "</VarDecl>\n";
}

void VisitorXML::visit(ASTWhileNode* node){
	printTabs();
	astFile <<"<While>\n";
	tabCount+=1;
	printTabs();

	if(checkPointer(node->getExpression())){
		node->getExpression()->accept(this);
	}

	if(checkPointer(node->getBlock())){
		node->getBlock()->accept(this);
	}

	tabCount-=1;
	printTabs();
	astFile << "</While>\n";
}

void VisitorXML::visit(ASTBinaryExprNode* node){
	printTabs();
	//changing < and > since not accepted in xml
	std::string op = node->getOperator();
	std::string returnOp;
	if(op.compare(">")==0){
		returnOp = "&lt;";
	}else if (op.compare(">=")==0){
		returnOp = "&lt;=";
	}else if(op.compare("<")==0){
		returnOp = "&gt;";
	}else if (op.compare("<=")==0){
		returnOp = "&gt;=";
	}
	astFile << "<BinExprNode Operator=\""+returnOp+"\">\n";
	tabCount+=1;

	if(checkPointer(node->getLeftExpression())){
		node->getLeftExpression()->accept(this);
	}

	if(checkPointer(node->getRightExpression())){
		node->getRightExpression()->accept(this);
	}
	tabCount-=1;
	printTabs();
	astFile << "</BinExprNode>\n";
}

void VisitorXML::visit(ASTBlockNode* node){
	printTabs();
	astFile << "<Block>\n";
	tabCount+=1;

	std::vector<ASTStmntNode*> localVec = node -> getStatements();
	for (ASTStmntNode* currentPtr : localVec){
		if(checkPointer(currentPtr)){
			currentPtr->accept(this);
		}
	}
	tabCount-=1;
	printTabs();
	astFile << "</Block>\n";
}

void VisitorXML::visit(ASTBooleanLiteralNode* node){
	printTabs();
	std::string local;

	if (node -> getBooleanValue()){
		local = "True";
	}else{
		local = "False";
	}
	astFile<<"<BooleanLiteral>"+local+"</BooleanLiteral>\n";
}

//--MISC

//prints tabs
void VisitorXML::printTabs(){
	for (int i = 0; i<tabCount;i++){
		astFile<<"\t";
	}
}

//print error mess
void VisitorXML::printError(){
	astFile << "\n";
	printTabs();
	astFile<<"<Error In Building AST>\n";
}

//checks if a pointer is a nullptr
bool VisitorXML::checkPointer(ASTNode* node){
	if (node == nullptr){
		return false;
	}else{
		return true;
	}
}

//goes through all statement nodes which make up the program
void VisitorXML::writeXML(std::vector<ASTStmntNode*> statements){
	astFile.open("ASTFile.xml");
	if(!astFile.is_open()){
		std::cout << "Unable To Write XML File"<<std::endl;
	}

	//initialise tabcount to 1 - at 0 we write root
	tabCount = 1;

	astFile << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"; 
	astFile << "<Program>\n";

	for (ASTStmntNode* currentPtr: statements){
		if(checkPointer(currentPtr)){
			currentPtr->accept(this);
		}else{
			printError();
		}
	}

	astFile << "</Program>\n";
	astFile.close();
	std::cout << "ASTFile.xml built!" << endl;
}
