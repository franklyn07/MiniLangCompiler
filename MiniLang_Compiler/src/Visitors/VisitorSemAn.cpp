#include "VisitorSemAn.h"
#include "../ASTNodes/ASTNode.h"
#include "../ASTNodes/ASTStmntNode.h"
#include "../ASTNodes/ASTExprNode.h"
#include "../ASTNodes/ASTVariableDeclarationNode.h"
#include "../ASTNodes/ASTAssignmentNode.h"
#include "../ASTNodes/ASTPrintNode.h"
#include "../ASTNodes/ASTIfNode.h"
#include "../ASTNodes/ASTWhileNode.h"
#include "../ASTNodes/ASTReturnNode.h"
#include "../ASTNodes/ASTFuncDeclNode.h"
#include "../ASTNodes/ASTBlockNode.h"
#include "../ASTNodes/ASTBinaryExprNode.h"
#include "../ASTNodes/ASTBooleanLiteralNode.h"
#include "../ASTNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ASTIdentifierNode.h"
#include "../ASTNodes/ASTRealLiteralNode.h"
#include "../ASTNodes/ASTIntegerLiteralNode.h"
#include "../ASTNodes/ASTStringLiteralNode.h"
#include "../ASTNodes/ASTUnaryNode.h"
#include "../ASTNodes/ASTFormalParamNode.h"
#include "../ASTNodes/ASTFormalParamsNode.h"
#include <iostream>


void VisitorSemAn::visit(ASTPrintNode* node) {
	//check expression correctness
	node -> getExpression() -> accept(this);
}

//check if id exists and if yes, check if it corresponds to the literal type
void VisitorSemAn::visit(ASTAssignmentNode* node){
	//find if id is already declared
	Type* t = sst->findEntity(node->getIdentifier()->getIdentifier());

	//if it is it will return a type else it will return a type with a null
	if(t->myType.compare("NULL")==0){
		throw std::runtime_error("Error! " +node->getIdentifier()->getIdentifier() + " was not declared yet!\n");
	}

	//check expression type
	node->getExpression()->accept(this);

	//now compare token with expression type
	if(!t->compareTokenType(expressionType)){
		throw std::runtime_error("Error! " + t->myType + " Value Expected For Variable " + node->getIdentifier()->getIdentifier() +"!\n");
	}
}

//check if the expressions have matching types to enable operation
void VisitorSemAn::visit(ASTBinaryExprNode* node){
	//will hold expression types
	std::string leftExpressionType;
	std::string rightExpressionType;

	//get expression types
	node->getLeftExpression()->accept(this);
	leftExpressionType = expressionType;
	node->getRightExpression()->accept(this);
	rightExpressionType = expressionType;

	if(!checkBinary(leftExpressionType,rightExpressionType,node->getOperator())){
		throw std::runtime_error("Error! Binary Expression Not Supported\n");
	}

}

//checks if binary operation can occur
bool VisitorSemAn::checkBinary(std::string leftExpression, std::string rightExpression, std::string Op){
	//handling all types of operators
	//*,/,- can only be handled by int and real
	if(Op.compare("*")==0||Op.compare("/")==0||Op.compare("-")==0){
		//if any one of the expressions is a string or boolean return false
		if(leftExpression.compare("Bool")==0||leftExpression.compare("String")==0||
			rightExpression.compare("Bool")==0||rightExpression.compare("String")==0){
			return false;
		}else{
			//check if one of the tokens is real - if yes the expression token will be also real
			if(leftExpression.compare("Real")==0||rightExpression.compare("Real")==0){
				expressionType = "Real";
			}else{
				expressionType = "Int";
			}
			return true;
		}
	}else if(Op.compare("+")==0){
		//check + which can be handled by all except bool
		if(leftExpression.compare("Real")==0&&rightExpression.compare("Real")==0){
			expressionType = "Real";
			return true;
		}else if((leftExpression.compare("Real")==0&&rightExpression.compare("Int")==0)||
			(leftExpression.compare("Int")==0&&rightExpression.compare("Int")==0)){
			expressionType = "Real";
			return true;
		}else if (leftExpression.compare("Int")==0&&rightExpression.compare("Int")==0){
			expressionType = "Int";
			return true;
		}else if (leftExpression.compare("String")==0&&rightExpression.compare("String")==0){
			expressionType = "String";
			return true;
		}else{
			return false;
		}
	}else if(Op.compare("and")==0||Op.compare("or")==0){
		//both must be of type boolean
		if(leftExpression.compare("Bool")==0&&rightExpression.compare("Bool")==0){
			expressionType = "Bool";
			return true;
		}else{
			return false;
		}
	}

	//if none of the previous matched, it means we have a relational operator
	else if(Op.compare("<")==0||Op.compare(">")==0||Op.compare("<=")==0||Op.compare(">=")==0){
		//we can only compare integers and real numbers
		if(leftExpression.compare("Bool")==0||leftExpression.compare("String")==0||
			rightExpression.compare("Bool")==0||rightExpression.compare("String")==0){
			return false;
		}else{
			expressionType = "Bool";
			return true;
		}
	}else{
		//== and != can only operate on same types
		if(leftExpression.compare("String")==0&&rightExpression.compare("String")==0){
			expressionType = "Bool";
			return true;
		}else if(leftExpression.compare("Bool")==0&&rightExpression.compare("Bool")==0){
			expressionType = "Bool";
			return true;
		}else if(leftExpression.compare("Int")==0&&rightExpression.compare("Int")==0){
			expressionType = "Bool";
			return true;
		}else if(leftExpression.compare("Real")==0&&rightExpression.compare("Real")==0){
			expressionType = "Bool";
			return true;
		}else{
			return false;
		}
	}
}

void VisitorSemAn::visit(ASTBooleanLiteralNode* node){
	//set expression type to boolean
	expressionType = "Bool";
}

void VisitorSemAn::visit(ASTRealLiteralNode* node){
	//set expression type to real
	expressionType = "Real";
}

void VisitorSemAn::visit(ASTStringLiteralNode* node){
	//set expression type to string
	expressionType = "String";
}

void VisitorSemAn::visit(ASTIntegerLiteralNode* node){
	//set expression type to int
	expressionType = "Int";
}

//anytime we get a new block we add a new scope
void VisitorSemAn::visit(ASTBlockNode* node){
	struct Scope newScope;

	//add new scope
	sst->pushNewScope(newScope);

	//check if function - if function formalParamTypes wont be empty
	if (!formalParamTypes.empty()){
		//get all the formal params and push them on new scope
		for (const auto &pair : formalParamTypes) {
			sst->insertIdandType_CurrentScope(pair.first,pair.second);	
		}

		//empty temporary map
		formalParamTypes.clear();
	}

	//visit all statements in new scope
	std::vector<ASTStmntNode*> localVec = node -> getStatements();
	for (ASTStmntNode* currentPtr : localVec){
		currentPtr->accept(this);
	}

	//close scope
	sst->popTopScope();
}

//if we find formal param check if the identifier already exists in that scope
void VisitorSemAn::visit(ASTFormalParamNode* node){
	Type* t = sst->findInScope(node->getIdentifier()->getIdentifier());

	//if found
	if(t->myType.compare("NULL")!=0){
		throw std::runtime_error("Error!"+node->getIdentifier()->getIdentifier()+" Identifier Already Exists In This Scope!\n");
		return;
	}

	//add identifier of parameter and its type to a temporary map
	formalParamTypes.insert({node->getIdentifier()->getIdentifier(),new Type(node->getTokenType())});

	//put parameter type on temporary vector for function
	temp.push_back(node->getTokenType());
}

//if formal params is found iterate through all formalparam
void VisitorSemAn::visit(ASTFormalParamsNode* node){
	std::vector<ASTFormalParamNode*> localVec = node -> getVector();
	for (ASTFormalParamNode* currentPtr : localVec){
		currentPtr->accept(this);
	}
}

//if identifier found check if it already exists if it doesnt throw error
void VisitorSemAn::visit(ASTIdentifierNode* node){
	Type* t = sst->findEntity(node->getIdentifier());

	//if not found
	if(t->myType.compare("NULL")==0){
		throw std::runtime_error("Error! "+node->getIdentifier()+" Cannot Find Variable!\n");
		return;
	}

	expressionType = t->myType;
}

//evaluate the expression - if it returns a boolean evaluate block 
//else throw error
void VisitorSemAn::visit(ASTWhileNode* node){
	node->getExpression()->accept(this);

	if (expressionType.compare("Bool")!=0){
		throw std::runtime_error("Error! Boolean Expression Expected in While\n");
		return;
	}

	node->getBlock()->accept(this);
}

void VisitorSemAn::visit(ASTVariableDeclarationNode* node){
	//check if the identifier exists in current scope
	Type* t = sst->findInScope(node->getIdentifier()->getIdentifier());

	if(t->myType.compare("NULL")!=0){
		throw std::runtime_error("Error!" + node->getIdentifier()->getIdentifier() + " Is Already Declared In This Scope!\n");
		return;
	}

	//get expression type
	node->getExpression()->accept(this);

	//if types match or expected a real and we get an int accept
	if(expressionType.compare(node->getTokenType())==0){
		//add identifier to current scope
		sst->insertIdandType_CurrentScope(node->getIdentifier()->getIdentifier(),new Type(node->getTokenType()));
	}else if (expressionType.compare("Int")==0&&node->getTokenType().compare("Real")==0){
		sst->insertIdandType_CurrentScope(node->getIdentifier()->getIdentifier(),new Type("Real"));
	}else{
		throw std::runtime_error("Error! Types Are Not Compatible! Expected: " + node->getTokenType() + " Recieved: " + expressionType +"\n");
	}
}

void VisitorSemAn::visit(ASTUnaryNode* node){
	//first check expression type
	node->getExpression()->accept(this);

	//since - can only be applied to integers and reals
	//whilst not can only be applied to booleans
	//check for this

	//if type boolean and not 'not'throw error
	if(expressionType.compare("Bool")==0){
		if(node->getString().compare("not")!=0){
			throw std::runtime_error("Error! 'not' Expected In Front Of Boolean Expression!\n");
		}
	}else if (expressionType.compare("Int")==0 || expressionType.compare("Real")==0){
		// if int or real operator must be '-' or else error
		if(node->getString().compare("-")!=0){
			throw std::runtime_error("Error! '-' Expected In Front Of Real/Int!\n");
		}
	}else{
		throw std::runtime_error("Error! Unsupported Type!\n");
	}
}

void VisitorSemAn::visit(ASTFunctionCallNode* node){
	//create vector of types to hold parameter types
	std::vector<std::string>myTypes;

	//check if identifier has been declared
	Type* t = sst->findEntity(node->getIdentifier()->getIdentifier());

	//if not declared
	if(t->myType.compare("NULL")==0){
		throw std::runtime_error("Error!" + node->getIdentifier()->getIdentifier() + " Function Not Declared!\n");
		return;
	}

	if(node->getFirstExprNode()!=nullptr){
		//if declared store types of both expression
		node->getFirstExprNode()->accept(this);
		std::string leftExpression = expressionType;
		myTypes.push_back(leftExpression);
	}

	//check whether there is more than one parameter
	if(node->getSecondExprNode()!=nullptr){
		node->getSecondExprNode()->accept(this);
		std::string rightExpression = expressionType;
		myTypes.push_back(rightExpression);
	}


	//parameters dont match with the expected ones 
	if(!t->compareParams(myTypes)){
		throw std::runtime_error("Error! Parameters of Function Don't Match With Those Expected!\n");
		return;
	}

	//clearing vector
	myTypes.clear();

	//else set return type of function call
	expressionType = t->myType;
}

void VisitorSemAn::visit(ASTIfNode* node){
	//check that expression returns a boolean
	node->getExpression()->accept(this);
	if(expressionType.compare("Bool")!=0){
		throw std::runtime_error("Error! If Condition Expects A Boolean Expression!\n");
		return;
	}

	//visit lBlock
	node->getLeftBlock()->accept(this);

	//if not null visit else block
	if(node->getRightBlock()!=nullptr){
		node->getRightBlock()->accept(this);
	}
}

void VisitorSemAn::visit(ASTReturnNode* node){
	//increase number of returns to check only one return/function
	numReturns+=1;

	//if no function declarations are open, you are returning without a function
	if(openFunctions<1){
		throw std::runtime_error("Error! Returning Without A Function!\n");
		return;
	}

	//if number of returns exceeds number of open functions - there are extra returns
	if(numReturns > openFunctions){
		throw std::runtime_error("Error! Only One Return Expected Per Function\n");
		return;
	}

	//check expression type
	node->getExpression()->accept(this);

	if(functionDeclReturnType.compare(expressionType)!=0){
		throw std::runtime_error("Error! Unexpected Return From Function! Expected: " + functionDeclReturnType + " Obtained: "+ expressionType+"\n");
		return;
	}

	//decreasing number of returns
	numReturns-=1;
}

void VisitorSemAn::visit(ASTFuncDeclNode* node){
	//increase number of open functions
	openFunctions+=1;

	//check if function name already exists
	Type* t = sst->findEntity(node->getIdentifier()->getIdentifier());

	if(t->myType.compare("NULL")!=0){
		throw std::runtime_error("Error! Function Already Declared!\n");
		return;
	}

	///get formal params
	node->getFormalParams()->accept(this);

	//setting return type of token for future return
	functionDeclReturnType = node->getTokenType();

	//push identifier on the current scope and with it add the vector of types expected
	sst->insertIdandType_CurrentScope(node->getIdentifier()->getIdentifier(),new Type(node->getTokenType(), createCopyVector(temp)));

	//clean temporary vector after storing the types of the parameters
	temp.clear();

	//visit block
	node -> getBlock() -> accept(this);

	//decrease number of open functions
	openFunctions-=1;
}

std::vector<std::string> VisitorSemAn::createCopyVector(std::vector<std::string> myVector){
	//will hold popped strings
	std::stack<std::string>myStack;

	//will hold copy of vector
	std::vector<std::string>copyVector;

	//after vector is emptied pop stack contents in new vector
	while(myVector.size()!=0){
		myStack.push(myVector.back());
		myVector.pop_back();
	}

	//this method ensures we keep the same order

	while(!myStack.empty()){
		copyVector.push_back(myStack.top());
		myStack.pop();
	}

	return copyVector;
}


//goes through all statement nodes which make up the program
bool VisitorSemAn::analyze(std::vector<ASTStmntNode*> statements){
	openFunctions = 0;
	numReturns = 0;
	functionDeclReturnType = "NULL";
	for (ASTStmntNode* currentPtr: statements){
		if(currentPtr != nullptr){
			try{
				currentPtr->accept(this);
			}catch(std::runtime_error &e){
				std::cout<<e.what();
				return false;
			}catch (std::bad_alloc& ba){
			    std::cerr << "bad_alloc caught: " << ba.what() << '\n';
				return false;
			}
		}
	}

	std::cout << "Semantic Analysis Performed!" << std::endl;
	return true;
}

void VisitorSemAn::clearScopeAnalyzer(){
	//empty temp
	while(!temp.empty()){
		temp.pop_back();
	}

	//empty formalParamTypes
	formalParamTypes.clear();

	sst->clearScope();
}