#include "VisitorInterpreter.h"
#include "../ASTNodes/ASTNode.h"
#include "../ASTNodes/ASTStmntNode.h"
#include "../ASTNodes/ASTExprNode.h"
#include "../ASTNodes/ASTVariableDeclarationNode.h"
#include "../ASTNodes/ASTAssignmentNode.h"
#include "../ASTNodes/ASTPrintNode.h"
#include "../ASTNodes/ASTIfNode.h"
#include "../ASTNodes/ASTWhileNode.h"
#include "../ASTNodes/ASTReturnNode.h"
#include "../ASTNodes/ASTFuncDeclNode.h"
#include "../ASTNodes/ASTBlockNode.h"
#include "../ASTNodes/ASTBinaryExprNode.h"
#include "../ASTNodes/ASTBooleanLiteralNode.h"
#include "../ASTNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ASTIdentifierNode.h"
#include "../ASTNodes/ASTRealLiteralNode.h"
#include "../ASTNodes/ASTIntegerLiteralNode.h"
#include "../ASTNodes/ASTStringLiteralNode.h"
#include "../ASTNodes/ASTUnaryNode.h"
#include "../ASTNodes/ASTFormalParamNode.h"
#include "../ASTNodes/ASTFormalParamsNode.h"
#include <iostream>


void VisitorInterpreter::visit(ASTPrintNode* node) {
	//interpret expression and overwrite lastValue
	node -> getExpression() -> accept(this);

	//print value obtained
	std::cout << lastValue ->getValue() <<std::endl;
}

//check if id exists and if yes, check if it corresponds to the literal type
void VisitorInterpreter::visit(ASTAssignmentNode* node){
	//interpret expression and overwrite lastValue
	node->getExpression()->accept(this);

	//update value of id in whichever scope it is found
	interTable->updateIdGlobal(node->getIdentifier()->getIdentifier(),new infoTuple(lastValue->getValue(),lastValue->getTokenType()));
}


//check if the expressions have matching types to enable operation
void VisitorInterpreter::visit(ASTBinaryExprNode* node){
	//will hold expression info
	infoTuple leftExpressionInfo;
	infoTuple rightExpressionInfo;

	//get expression types
	node->getLeftExpression()->accept(this);
	leftExpressionInfo = *lastValue;
	node->getRightExpression()->accept(this);
	rightExpressionInfo = *lastValue;

	workBinary(leftExpressionInfo,rightExpressionInfo,node->getOperator());

}

//works a binary operation
void VisitorInterpreter::workBinary(infoTuple leftExpression, infoTuple rightExpression, std::string Op){
	//handling all types of operators
	//*,/,- can only be handled by int and real
	if(Op.compare("*")==0||Op.compare("/")==0||Op.compare("-")==0){
		//we assume only integers and reals will be passed 
		Token leftToken = leftExpression.getTokenType();
		Token rightToken = rightExpression.getTokenType();
		//if both are integers we will return an integer else a real
		if(leftToken == Token::Tok_TypeInt && rightToken == Token::Tok_TypeInt){
			//convert string values to actual values
			int leftVal = std::stoi(leftExpression.getValue());
			int rightVal = std::stoi(rightExpression.getValue());
			//will hold the answer
			int ans;
			if(Op.compare("*")==0){
				ans = leftVal * rightVal;
			}else if(Op.compare("/")==0){
				if(rightVal == 0){
					throw std::runtime_error("Cannot Divide By Zero!");
				}else{
					ans = leftVal / rightVal;
				}
			}else if (Op.compare("-")==0){
				ans = leftVal-rightVal;
			}
			lastValue = new infoTuple(std::to_string(ans), Token::Tok_TypeInt);
		}else{
			//convert string values to actual values
			float leftVal = std::stof(leftExpression.getValue());
			float rightVal = std::stof(rightExpression.getValue());
			//will hold the answer
			float ans;
			if(Op.compare("*")==0){
				ans = leftVal * rightVal;
			}else if(Op.compare("/")==0){
				if(rightVal == 0){
					throw std::runtime_error("Cannot Divide By Zero!");
				}else{
					ans = leftVal / rightVal;
				}
			}else if (Op.compare("-")==0){
				ans = leftVal-rightVal;
			}
			lastValue = new infoTuple(std::to_string(ans), Token::Tok_TypeReal);
		}
	}else if(Op.compare("+")==0){
		//check + which can be handled by all except bool
		if(leftExpression.getTokenType() == Token::Tok_TypeReal &&rightExpression.getTokenType() == Token::Tok_TypeReal ){
			//convert string values to actual values
			float leftVal = std::stof(leftExpression.getValue());
			float rightVal = std::stof(rightExpression.getValue());
			//will hold the answer
			float ans;
			ans = leftVal + rightVal;
			lastValue = new infoTuple(std::to_string(ans), Token::Tok_TypeReal);
		}else if((leftExpression.getTokenType() == Token::Tok_TypeReal&&rightExpression.getTokenType() == Token::Tok_TypeInt)||
			(rightExpression.getTokenType() == Token::Tok_TypeReal&&leftExpression.getTokenType() == Token::Tok_TypeInt)){
			//convert string values to actual values
			float leftVal = std::stof(leftExpression.getValue());
			float rightVal = std::stof(rightExpression.getValue());
			//will hold the answer
			float ans;
			ans = leftVal + rightVal;
			lastValue = new infoTuple(std::to_string(ans), Token::Tok_TypeReal);
		}else if (leftExpression.getTokenType() == Token::Tok_TypeInt&&rightExpression.getTokenType() == Token::Tok_TypeInt){
			//convert string values to actual values
			int leftVal = std::stoi(leftExpression.getValue());
			int rightVal = std::stoi(rightExpression.getValue());
			//will hold the answer
			int ans;
			ans = leftVal + rightVal;
			lastValue = new infoTuple(std::to_string(ans), Token::Tok_TypeInt);
		}else if (leftExpression.getTokenType() == Token::Tok_TypeString&&rightExpression.getTokenType() == Token::Tok_TypeString){
			std::string leftVal = leftExpression.getValue();
			std::string rightVal = rightExpression.getValue();
			std::string ans = leftVal + rightVal;
			lastValue = new infoTuple(ans, Token::Tok_TypeString);  
		}
	}else if(Op.compare("and")==0){
		//will hold temporary value
		std::string temp;

		//will hold actual boolean values
		bool leftVal;
		bool rightVal;

		//get leftVal
		temp = leftExpression.getValue();
		if(temp.compare("True")==0){
			leftVal = true;
		}else{
			leftVal = false;
		}

		//get right val
		temp = rightExpression.getValue();
		if(temp.compare("True")==0){
			rightVal = true;
		}else{
			rightVal = false;
		}

		bool answer = leftVal and rightVal;
		std::string ans;
		if(answer){
			ans = "True";
		}else{
			ans = "False";
		}
		lastValue = new infoTuple(ans, Token::Tok_TypeBool);
	}else if(Op.compare("or")==0){
		//will hold temporary value
		std::string temp;

		//will hold actual boolean values
		bool leftVal;
		bool rightVal;

		//get leftVal
		temp = leftExpression.getValue();
		if(temp.compare("True")==0){
			leftVal = true;
		}else{
			leftVal = false;
		}

		//get right val
		temp = rightExpression.getValue();
		if(temp.compare("True")==0){
			rightVal = true;
		}else{
			rightVal = false;
		}

		bool answer = leftVal or rightVal;
		std::string ans;
		if(answer){
			ans = "True";
		}else{
			ans = "False";
		}
		lastValue = new infoTuple(ans, Token::Tok_TypeBool);
	}

	//if none of the previous matched, it means we have a relational operator
	else if(Op.compare("<")==0||Op.compare(">")==0||Op.compare("<=")==0||Op.compare(">=")==0){
		//store values as real since it can hold both reals and ints
		float leftVal = std::stof(leftExpression.getValue());
		float rightVal =std::stof(rightExpression.getValue());

		//will hold boolean answer
		bool answer;

		if(Op.compare("<")==0){
			if(leftVal<rightVal){
				answer = true;
			}else{
				answer = false;
			}
		}else if(Op.compare(">")==0){
			if(leftVal>rightVal){
				answer = true;
			}else{
				answer = false;
			}
		}else if(Op.compare("<=")==0){
			if(leftVal<=rightVal){
				answer = true;
			}else{
				answer = false;
			}
		}else if(Op.compare(">=")==0){
			if(leftVal>=rightVal){
				answer = true;
			}else{
				answer = false;
			}
		}

		std::string ans;
		if(answer){
			ans = "True";
		}else{
			ans = "False";
		}
		lastValue = new infoTuple(ans, Token::Tok_TypeBool);

	}else{
		if(Op.compare("!=")==0){
			if(leftExpression.getValue().compare(rightExpression.getValue())!=0){
				lastValue = new infoTuple("True", Token::Tok_TypeBool);
			}else{
				lastValue = new infoTuple("False", Token::Tok_TypeBool);
			}
		}else{
			if(leftExpression.getValue().compare(rightExpression.getValue())==0){
				lastValue = new infoTuple("True", Token::Tok_TypeBool);
			}else{
				lastValue = new infoTuple("False", Token::Tok_TypeBool);
			}
		}
	}
}

void VisitorInterpreter::visit(ASTBooleanLiteralNode* node){
	bool value = node ->getBooleanValue();

	//will hold boolean value in string
	std::string booleanVal;

	if(value){
		booleanVal = "True";
	}else{
		booleanVal = "False";
	}

	lastValue = new infoTuple(booleanVal,Token::Tok_TypeBool);
}

void VisitorInterpreter::visit(ASTRealLiteralNode* node){
	lastValue = new infoTuple(std::to_string(node->getRealValue()),Token::Tok_TypeReal);
}

void VisitorInterpreter::visit(ASTStringLiteralNode* node){
	lastValue = new infoTuple(node->getString(),Token::Tok_TypeString);

}

void VisitorInterpreter::visit(ASTIntegerLiteralNode* node){
	lastValue = new infoTuple(std::to_string(node->getInteger()),Token::Tok_TypeInt);

}

//anytime we get a new block we add a new scope
void VisitorInterpreter::visit(ASTBlockNode* node){
	struct interpreterScope newScope;

	//will determine whether we need to pop scope or not
	//if not a function block don't pop (function will take
	//care of it), else pop
	bool localFlag = false;

	//check if function block
	if(!functionFlag){
		//if not function block pop new scope and 
		//raise flag to pop it after completion of block
		interTable->pushNewInterpreterScope(newScope);
		localFlag = true;
	}

	//visit all statements
	std::vector<ASTStmntNode*> localVec = node -> getStatements();

	for (ASTStmntNode* currentPtr : localVec){
		currentPtr->accept(this);
	}

	//if a scope was pushed - pop it after finishing statements
	if(localFlag){
		interTable->popTopInterpreterScope();
	}	
}

//if we find formal param check if the identifier already exists in that scope
void VisitorInterpreter::visit(ASTFormalParamNode* node){
	std::string type = node -> getTokenType();

	//will hold token to pass as a parameter
	Token tokenType = Token::Tok_ERROR;
	if(type.compare("Real")==0){
		tokenType = Token::Tok_TypeReal;
	}else if(type.compare("Int")==0){
		tokenType = Token::Tok_TypeInt;
	}else if(type.compare("Bool")==0){
		tokenType = Token::Tok_TypeBool;
	}else if(type.compare("String")==0){
		tokenType = Token::Tok_TypeString;
	}

	//putting id and type of formal param on current scope
	interTable->insertIdandType_CurrentScope(node->getIdentifier()->getIdentifier(),new infoTuple(tokenType));

	//pushing parameters on stack
	stackOfParams.push(node->getIdentifier()->getIdentifier());
}

//if formal params is found iterate through all formalparam
void VisitorInterpreter::visit(ASTFormalParamsNode* node){
	std::vector<ASTFormalParamNode*> localVec = node -> getVector();
	for (ASTFormalParamNode* currentPtr : localVec){
		currentPtr->accept(this);
	}
}

void VisitorInterpreter::visit(ASTIdentifierNode* node){
	//search for identifier in all scopes
	infoTuple local = *interTable->findEntity(node->getIdentifier());

	//check if identifier already exists
	//if it already exists set lastVal to its value
	//else add it to currentscope 
	if (local.getValue().compare("NULL")!=0){
		lastValue = new infoTuple(local.getValue(),local.getTokenType());
	}else{
		interTable->insertIdandType_CurrentScope(node->getIdentifier(),new infoTuple());
	}

}

//evaluate expression and loop while it is true
void VisitorInterpreter::visit(ASTWhileNode* node){
	node->getExpression()->accept(this);

	//if expression evaluates to be true iterate
	while(lastValue->getValue().compare("True")==0){
		//visit while block
		node->getBlock()->accept(this);
		//update expression to check if it is still valid
		node->getExpression()->accept(this);
	}

}

void VisitorInterpreter::visit(ASTVariableDeclarationNode* node){
	//evaluate expression
	node->getExpression()->accept(this);

	//assign result to id in current scope
	interTable->insertIdandType_CurrentScope(node -> getIdentifier() -> getIdentifier(),new infoTuple(lastValue->getValue(),lastValue->getTokenType()));

}

void VisitorInterpreter::visit(ASTUnaryNode* node){
	//first check expression
	node->getExpression()->accept(this);

	//if expression is of boolean type
	if (lastValue->getTokenType() == Token::Tok_TypeBool){
		if(lastValue->getValue().compare("True")==0){
			lastValue = new infoTuple("False",Token::Tok_TypeBool);
		}else{
			lastValue = new infoTuple("True",Token::Tok_TypeBool);
		}
	}else{
		if(lastValue->getTokenType() == Token::Tok_TypeReal){
			float value = std::stof(lastValue -> getValue());
			value = -1 * value;
			lastValue = new infoTuple(std::to_string(value),Token::Tok_TypeReal);
		}else{
			int value = std::stoi(lastValue -> getValue());
			value = -1 * value;
			lastValue = new infoTuple(std::to_string(value),Token::Tok_TypeInt);
		}
	}
}

void VisitorInterpreter::visit(ASTFunctionCallNode* node){
	//stack to store values of parameters
	std::stack<infoTuple> paramValues;

	//set function flag to true
	functionFlag = true;

	//limited to two parameters - further improvement increase number of params
	if(node->getFirstExprNode()!=nullptr){
		node->getFirstExprNode()->accept(this);
		paramValues.push(*lastValue);
	}

	//check whether there is more than one parameter
	if(node->getSecondExprNode()!=nullptr){
		node->getSecondExprNode()->accept(this);
		paramValues.push(*lastValue);
	}	

	//create new scope
	struct interpreterScope newScope;

	//push new scope for function
	interTable->pushNewInterpreterScope(newScope);

	//get info about function from global
	infoTuple functionInfo = *interTable->findEntity(node->getIdentifier()->getIdentifier());

	//visit its formal params in scope and add them to current
	functionInfo.getParameters()->accept(this);	

	//empty stack of parameters filled by formalparam nodes
	while(!stackOfParams.empty()){
		//find id of parameter in current scope
		//and update its info with the new info
		interTable->updateIdInScope(stackOfParams.top(),&paramValues.top());
		stackOfParams.pop();
		paramValues.pop();
	}

	//visit block to execute statements
	functionInfo.getBlock()->accept(this);


	//close scope
	interTable->popTopInterpreterScope();

	//reset flag
	functionFlag = false;
}

void VisitorInterpreter::visit(ASTIfNode* node){
	//evaluate expression
	node->getExpression()->accept(this);
	
	//if expression is true 
	if (lastValue->getValue().compare("True")==0){
		node->getLeftBlock()->accept(this);
	}else{
		//check if there is an else block
		if(node->getRightBlock() != nullptr){
			//if there is visit it
			node->getRightBlock()->accept(this);
		}
	}

}

void VisitorInterpreter::visit(ASTReturnNode* node){
	//just visit expression
	node->getExpression()->accept(this);
}

void VisitorInterpreter::visit(ASTFuncDeclNode* node){
	//add function declaration to current scope
	//storing its information
	interTable->insertIdandType_CurrentScope(node->getIdentifier()->getIdentifier(),new infoTuple(node->getFormalParams(),node->getBlock()));
}


//goes through all statement nodes which make up the program
bool VisitorInterpreter::interpret(std::vector<ASTStmntNode*> statements){
	functionFlag  = false;
	lastValue = nullptr;

	for (ASTStmntNode* currentPtr: statements){
		if(currentPtr != nullptr){
			try{
				currentPtr->accept(this);
			}catch(std::runtime_error &e){
				std::cerr<<e.what();
				return false;
			}catch (std::bad_alloc& ba){
			    std::cerr << "bad_alloc caught: " << ba.what() << '\n';
				return false;
			}catch (std::invalid_argument& ia){
			    std::cerr << "invalid_alloc caught: " << ia.what() << '\n';
				return false;
			}catch(std::out_of_range& oor){
			    std::cerr << "out_of_range caught: " << oor.what() << '\n';
				return false;
			}
		}
	}

	std::cout << "Interpretation Performed!" << std::endl;
	return true;
}

//cleanup
void VisitorInterpreter::clearScopeInterpreter(){
	while(!stackOfParams.empty()){
		stackOfParams.pop();
	}
	interTable->clearScope();
}