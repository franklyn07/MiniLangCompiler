{
	print Ed;
	{
		print Tom;
		{
			print Frank;
		}
		print youAre;
	}
}

{
	set noName = Ed;
	{
		set yourName = Tom;
		{
			set myName = Frank;
		}
		set willIAm = youAre;
	}
}

return myName;