def funcSquare(x: real) : real {
    return x*x;
}

/*multiple
lines
comment*/

def funcGreaterThan(z:real , y:real) : bool{
    var ans : bool = true;
    if(y>z){
        set ans = false;
    }
    return ans;
}


//print y;
var l: real = 2.4;

var m : real = funcSquare(5);

while(not l>m){
    print "hello";
    set l = l + 5;    
}

print funcSquare(3);

//print y;
print funcGreaterThan(l, 2.3); //inline comment
print funcGreaterThan(l, funcSquare(1.555));
print l;
