src - contains all the source code written
DevTests - contains the bits and pieces of code, used to test the compiler throughout the development cycle
FinalTests - contains the largest programs which were used to test the compiler, together with the output
XMLFiles - contains the xml files produced by the compiler for the specific tests found in FinalTests
