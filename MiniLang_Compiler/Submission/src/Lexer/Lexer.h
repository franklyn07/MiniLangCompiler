#ifndef LEXER_H
#define LEXER_H
#include "Token.h"
#include "charLabel.h"
#include "Transitions.h"
#include <string>
#include <stack>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

struct token{
	string lexeme;
	Token tokenType;
	string valueDfa;
};

class Lexer{

	private:
		//gets character form file
		char getChar();
		//closes file
		void closeFile();
		//clears the stack
		void stackClear(stack<int>  &myStack);
		//determines the character type
		charLabel charType(char inputChar);
		//performs a search on the stack
		bool searchStack(int state, stack<int> stackCopy);
		//gives next token
		token NextToken();
		//holds all the tokens produced
		vector<token> vectorOfTokens;

	public:
		
		//constructor
		Lexer(){};
		//tokenizes program returns true upon success
		bool tokenizeProgram(std::string path);
		//shows next token
		token previewNextToken();
		//gets next token
		token getCurrentToken();
		//get whole vector of tokens
		vector<token> getVectorOfTokens();
		//perform cleanup
		void cleanVectorOfTokens();
		~Lexer(){};
};

#endif