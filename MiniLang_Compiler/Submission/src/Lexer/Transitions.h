#ifndef TRANSITIONS_H
#define TRANSITIONS_H

#include "charLabel.h"
#include "Token.h"
#include <string>
#include <unordered_map>

using namespace std;

class Transitions{
	public:
		//constructor
		Transitions(){};
		//performs transitions form one state to another
		int transitionFunc(int state, charLabel label);
		//detects whether a state is an accept state or not
		bool isAccept(int State);
		//returns token type after completion of transitions
		Token returnToken(int State, string lexeme);

	private:
		//detects if a string is a keyword or not
		Token checkTokenType(string lexeme);
};

#endif