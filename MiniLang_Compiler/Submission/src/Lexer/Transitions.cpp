#include "Transitions.h"

int transitionTable [20] [14] = {	

//State Number is Column number
//Input Char in First Row - For Proper Indexint see charLabel 

//	|Dig|ADD|Mult|DP|<>	|Punc|_	|Alph|"	| /	| =	| !	|12	|NL
	{1 , 4	, 5	,-1	,12	,15	,11	,11	,8 	,6	,14	,12	,-1	,-1},
	{1 , -1	, -1,2	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{3 , -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{3 , -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, 16,-1	,-1	,-1	,-1	,-1	,-1	,7	,-1	,-1	,-1	,-1},
	{7 , 7	, 17,7 	,7	,7	,7	,7	,7	,7	,7	,7	,7	,19},
	{9 , 9	, 9	,9 	,9	,9	,9	,9	,9	,9	,9	,9	,9	,-1},
	{9 , 9	, 9	,9 	,9	,9	,9	,9	,10	,9	,9	,9	,9	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{11, -1	, -1,-1	,-1	,-1	,11	,11	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,13	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,13	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{16, 16 , 17,16 ,16 ,16 ,16 , 16,16 ,16 ,16 ,16 ,16 ,16},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,18	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1},
	{-1, -1	, -1,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1	,-1}
	};


//performs transitions from one state to another
int Transitions::transitionFunc(int state, charLabel label){

	int convertedIndex = (int)label;	

	//if invalid state, or undefined char stop loop
	if(state == -1 ||convertedIndex == -1){
		return -1;
	}else{
		return transitionTable[state][convertedIndex];
	}
}

//marks which states are accept states
bool Transitions::isAccept(int State){
	switch(State){
		case 0:
		case 2:
		case 7:
		case 8:
		case 9:
		case 16:
		case 17:
			return false;
		case 1:
		case 3:
		case 4:
		case 5:
		case 6:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 18:
		case 19:
			return true;
		default:
			return false;
	};
}

//returns token type according to accept state
Token Transitions::returnToken(int State, string lexeme){
	switch(State){
		case 1:
			return Token::Tok_TypeInt;
		case 3:
			return Token::Tok_TypeReal;
		case 4:
			return Token::Tok_AddOp;
		case 5:
		case 6:
			return Token::Tok_MultOp;
		case 10:
			return Token::Tok_TypeString;
		case 11:
			return checkTokenType(lexeme);
		case 12:
		case 13:
			return Token::Tok_RelOp;
		case 14:
			return Token::Tok_AssignmentOp;
		case 15:
			return Token::Tok_Punctuation;
		case 18:
			return Token::Tok_BlockComment;
		case 19:
			return Token::Tok_Comment;
		default:
			return Token::Tok_ERROR;
	}
}

Token Transitions::checkTokenType(string lexeme){
	unordered_map<string,Token> myDictionary = {
		{"real", Token:: Tok_KW_real},
		{"int", Token:: Tok_KW_int},
		{"bool", Token:: Tok_KW_bool},
		{"string", Token:: Tok_KW_string},
		{"false", Token:: Tok_TypeBool},
		{"true", Token:: Tok_TypeBool},
		{"not", Token:: Tok_KW_not},
		{"return", Token:: Tok_KW_return},
		{"and", Token:: Tok_KW_and},
		{"or", Token:: Tok_KW_or},
		{"if", Token:: Tok_KW_if},
		{"else", Token:: Tok_KW_else},
		{"set", Token:: Tok_KW_set},
		{"var", Token:: Tok_KW_var},
		{"print", Token:: Tok_KW_print},
		{"while", Token:: Tok_KW_while},
		{"def", Token:: Tok_KW_def}
	};

	unordered_map<string,Token>::const_iterator finder = myDictionary.find (lexeme);

  	if (finder == myDictionary.end() ){
    	return Token::Tok_Identifier;
  	}else{
    	return finder->second;
  	}
}
