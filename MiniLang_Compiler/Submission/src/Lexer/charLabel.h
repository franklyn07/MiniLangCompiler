#ifndef CHARLABEL_H
#define CHARLABEL_H

//purpose of this class is to serve as an index
//the values given are their position in the array

enum class charLabel{
	
	Undefined = -1,
	InvalidChar = 12,
	Digit =0,
	AddOp =1,
	MultOp =2,
	DecimalPoint =3,
	GTST = 4,
	Punctuation =5,
	Underscore = 6,
	Alphabet = 7,
	InvertedCommas = 8,
	DivOp = 9,
	Equals = 10,
	ExclamationMark = 11,
	NewLine = 13
};

#endif