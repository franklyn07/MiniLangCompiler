#include "Lexer.h"
#include "Debugging.h"

//reads from source file
ifstream textFile;

//keeps track of states
stack<int> stateStack;

//keeps current state number
int state;

//holds current lexeme
string lexeme;

//keeps track of whether lexeme was identified or not
bool keepTrack;

//initialising transitions class
Transitions trans;

//For Debugging purposes
Debugging d;


//gets next char from file    
char Lexer::getChar(){
	char x;
	x = textFile.get();
	return (char) x;
}



//closes file
void Lexer::closeFile(){
	textFile.close();
}

//clears stack and restores it to original state
void Lexer::stackClear(stack <int> &myStack){
	int myInt = 0;
	
	//if not empty, empty it
	if(!myStack.empty()){
		do{
			myInt = myStack.top();
			myStack.pop();
		}while(myInt != -2);
	}

	//Set -2 as a benchmark
	myStack.push(-2);
}


//labelling input characters into indexes
charLabel Lexer::charType(char inputChar){
	charLabel label;
	//getting integer value of char 
	int charToInt = (int)inputChar;
	if(charToInt >= 48 && charToInt <= 57){
		label = charLabel::Digit;
	}else if((charToInt >= 65 && charToInt <= 90) || (charToInt >= 97 && charToInt <= 122)){
		label = charLabel::Alphabet;
	}else if (charToInt == 46){
		label = charLabel::DecimalPoint;
	}else if (charToInt == 42){
		label = charLabel::MultOp;
	}else if (charToInt == 47){
		label = charLabel::DivOp;
	}else if (charToInt == 43 || charToInt == 45){
		label = charLabel::AddOp;
	}else if (charToInt == 60 || charToInt == 62){
		label = charLabel::GTST;
	}else if (charToInt == 95){
		label = charLabel::Underscore;
	}else if (charToInt == 34){
		label = charLabel::InvertedCommas;
	}else if (charToInt == 61){
		label = charLabel::Equals;
	}else if (charToInt == 33){
		label = charLabel::ExclamationMark;
	}else if (charToInt == 40 || charToInt == 41 || charToInt == 44 ||charToInt == 58 || charToInt == 59 ||charToInt == 123 || charToInt == 125){
		label = charLabel::Punctuation;
	}else if (charToInt == 10){
		label = charLabel::NewLine;
	}else if (charToInt >= 32 || charToInt<=126){
		/*this if condition, collects all the characters,
		which are in range of acceptance, yet they were not useful,
		thus seeped through the previous conditions*/
		label = charLabel::InvalidChar;
	}else{
		//this collects all the caracters not in our domain
		label = charLabel::Undefined;
	}
	return label;
}

//returns a token from the text file
token Lexer::NextToken(){
	//initialising lexeme, dfa state and stack
   	//return state to init state with every call to next token
    //clear stack before each token
    stackClear(stateStack);
	state = 0;
	lexeme = "";
	//by default lexeme is identified
	keepTrack = true;

	struct token myToken;

	while(state != -1){
		char currentChar = getChar();

		//check if eof is reached - if yes return appropriate token
		if(currentChar == EOF){
			//chek if lexeme is empty - if yes you can return EOF Token 
			//if not empty put back eof in file and return the current lexeme
			if(lexeme.length()!=0){
				//put back eof for reading
				textFile.unget();
			}else{
				//if lexeme is empty return eof token
				myToken.lexeme = "EOF";
				myToken.valueDfa = -2;
				myToken.tokenType = Token::Tok_EOF;
				return myToken;
			}
		}

		//if input is a space or nl or tab and we are not reading a comment or stringliteral,
		//break else perform normally
		if((int)currentChar == 32 || (int)currentChar == 9){
			if(!searchStack(8,stateStack) && !searchStack(16,stateStack) && !searchStack(7,stateStack)){
				break;
			}
		}

		/*new line is used as a delimeter for single line comments as well as part
		of the multiline's comment lexeme*/
		if((int)currentChar == 10|| (int)currentChar == 9){
			if(!searchStack(16,stateStack) && !searchStack(7,stateStack)){
				break;
			}
		}

		lexeme += currentChar;
		if(trans.isAccept(state)){
			stackClear(stateStack);
		}
		stateStack.push(state);
		state = trans.transitionFunc(state, charType(currentChar));
	}

	//hold copy of found lexeme - if not identified it will be returned not truncated
	string lexemeCopy = lexeme;

	//get copy of current position in file before rollback
	int currentPos = textFile.tellg();

	//rollback function to go back to accept state
	while(trans.isAccept(state)!=true && state != -2){
		state = stateStack.top();
		stateStack.pop();

		//put last character back in file for reading
		textFile.unget();

		//removing last character from lexeme if lexeme is not empty
		//if lexeme is empty it means it was not identified (all characters
		//removed yet no accept state)
		if(state == -2){
			keepTrack = false;

			//if lexeme was not identified go to saved position and keep reading from there
			//rather than tracking back to character put back
			textFile.seekg(currentPos);
		}else{
			lexeme.pop_back();
		}
	}

	//if lexeme is identified - store the truncated version
	//if not store its copy which is not truncated/rolledback upon
	if(keepTrack){
		myToken.lexeme = lexeme;
		myToken.tokenType = trans.returnToken(state,lexeme);
	}else{
		myToken.lexeme = lexemeCopy;
		myToken.tokenType = Token::Tok_ERROR;
	}

	//if identified token it will hold its state else it will hold -2
	myToken.valueDfa = state;

	return myToken;
}

//searches if an item is on the stack
bool Lexer::searchStack(int state, stack<int> stackCopy){
	while(!stackCopy.empty()){
		int currentState = stackCopy.top();
		stackCopy.pop();
		if (state == currentState){
			return true;
		}
	}

	return false;
}

//tokenizes the program
bool Lexer::tokenizeProgram(std::string filePath){
	token t;

	//Check whether source file can be opened
    textFile.open(filePath);
    if (!textFile) {
        cout << "Unable to open file";
    	return false;
    }

	do{
		t = NextToken();
		//if a token is empty or else is a comment, it is ignored
		if(t.lexeme.compare("") !=0){
			if(t.tokenType != Token::Tok_Comment && t.tokenType != Token::Tok_BlockComment){
				vectorOfTokens.push_back(t);
			}
		}
	}while(t.tokenType != Token::Tok_EOF);

	closeFile();

    cout << "Tokenization Of File Completed!"<<endl;
    return true;
}

//checks what the next token in line will be
token Lexer::previewNextToken(){
	return vectorOfTokens.front();
}

//gets the current token (the one before watching the next one)
token Lexer::getCurrentToken(){
	token currentTokenCopy = vectorOfTokens.front();
	vectorOfTokens.erase(vectorOfTokens.begin());
	return currentTokenCopy;
}

//getter for vector of tokens
vector<token> Lexer::getVectorOfTokens(){
	return vectorOfTokens;
}

//cleans vector of tokens
void Lexer::cleanVectorOfTokens(){
	while(!vectorOfTokens.empty()){
		vectorOfTokens.pop_back();
	}
}