//Used by VisitorSemAn.h

#include "./interpreterTable.h"
#include <iostream>

interpreterTable::interpreterTable(){
	//initialise a global scope
	struct interpreterScope global;

	//add global scope to stack of scopes
	pushNewInterpreterScope(global);
}

//destructor
interpreterTable::~interpreterTable(){
}

//push new scope on stack
void interpreterTable::pushNewInterpreterScope(interpreterScope s){
	table.push(s);
}

//pop the top scope from stack
void interpreterTable::popTopInterpreterScope(){
	table.pop();
}

//insert a type 
void interpreterTable::insertIdandType_CurrentScope(std::string id,infoTuple* it){
	table.top().scope.insert(std::make_pair(id,it));
}

//find item in map 
// if not found returns iterator to end, else returns iterator
infoTuple* interpreterTable::findEntity(std::string identifier){
	//will hold the scopes temporarily
	std::stack<interpreterScope> temp;

	//will hold iterator of map
	std::unordered_map<std::string,infoTuple*>::const_iterator it;

	while(!table.empty()){
		for(auto key : table.top().scope){
			if(key.first.compare(identifier)==0){
				if(!temp.empty()){
					while(!temp.empty()){
						table.push(temp.top());
						temp.pop();
					}
				};
				return key.second;
			}
		}
		//if not found
		//pop top scope and store it temporarily in temp
		temp.push(table.top());
		table.pop();
	}


	//repopulating symbol table if id is never found
	if(!temp.empty()){
		while(!temp.empty()){
			table.push(temp.top());
			temp.pop();
		}
	}
	std::cout<<"Nuller"<<std::endl;
	infoTuple* t = new infoTuple("NULL");
	//if nothing is returned up till now return null
	return t;
}

infoTuple* interpreterTable::findInScope(std::string identifier){
	for(auto key : table.top().scope){
		if(key.first.compare(identifier)==0){
			return key.second;
		}
	}

	infoTuple* t = new infoTuple("NULL");
	return t;
}

//will update info about id in global scope
void interpreterTable::updateIdGlobal(std::string identifier, infoTuple* info){
	//will hold the scopes temporarily
	std::stack<interpreterScope> temp;

	//will hold iterator of map
	std::unordered_map<std::string,infoTuple*>::const_iterator it;

	while(!table.empty()){
		//try to find the identifier in the current scope
		it = table.top().scope.find(identifier);

		//if identifier is found
		if (it != table.top().scope.end()){
			//update value if found
			table.top().scope[it->first] = info;

			//check whether temp is empty - if not put all scopes back in
			if(!temp.empty()){
				while(!temp.empty()){
					table.push(temp.top());
					temp.pop();
				}
			}
			return;
		}else{
			//if not found
			//pop top scope and store it temporarily in temp
			temp.push(table.top());
			table.pop();
		}
	}

	//repopulating symbol table if id is never found
	if(!temp.empty()){
		while(!temp.empty()){
			table.push(temp.top());
			temp.pop();
		}
	}

}

//will find an id in the current scope and replace its contents
//with the new contents
void interpreterTable::updateIdInScope(std::string identifier, infoTuple* info){
	//will hold iterator of map
	std::unordered_map<std::string,infoTuple*>::const_iterator it;

	it = table.top().scope.find(identifier);

	//if identifier is found
	if (it != table.top().scope.end()){
		//remove old value from map and add new value with new info
		table.top().scope[it->first] = info;
	}
}

void interpreterTable::printTopScope(){
	if(!table.empty()){
		for(auto key : table.top().scope){
			std::cout<<"Id: "<<key.first<<std::endl;
			std::cout<<"Value: "<<key.second->getValue()<<std::endl;
		}
	}else{
		std::cout<<"Empty Scope\n";
	}			
}

void interpreterTable::clearScope(){
	while(!table.empty()){
		table.top().scope.clear();
		table.pop();
	}

	//initialise a global scope
	struct interpreterScope global;

	//add global scope to stack of scopes
	pushNewInterpreterScope(global);
}