//Used by visitor interpreter

#ifndef INTERPRETERTABLE_H
#define INTERPRETERTABLE_H

#include <string>
#include <stack>
#include <unordered_map>
#include "./infoTuple.h"

struct interpreterScope{
	std::unordered_map<std::string, infoTuple*> scope;
};

class interpreterTable{
	private:
		//symbolTable holder
		std::stack<interpreterScope> table;

	public:
		//constructor
		interpreterTable();

		//destructor
		~interpreterTable();

		//push new scope on stack
		void pushNewInterpreterScope(interpreterScope s);

		//pop the top scope from stack
		void popTopInterpreterScope();

		//insert a type 
		void insertIdandType_CurrentScope(std::string id,infoTuple* info);

		//find item in map 
		//if found return Type else return Null
		infoTuple* findEntity(std::string identifier);

		//search only in current scope
		infoTuple* findInScope(std::string identifier);

		//will find an id in the global scope and replace its contents
		//with the new contents
		void updateIdGlobal(std::string identifier, infoTuple* info);

		//will find an id in the current scope and replace its contents
		//with the new contents
		void updateIdInScope(std::string identifier, infoTuple* info);

		//for debugging purposes
		void printTopScope();

		//clears stack of scopes
		void clearScope();
};

#endif