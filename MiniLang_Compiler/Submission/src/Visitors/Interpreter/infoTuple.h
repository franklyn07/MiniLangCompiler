//this class will hold the info of each id for interpreter

#ifndef INFOTUPLE_H
#define INFOTUPLE_H

#include <string>
#include "../../Lexer/Token.h"
#include "../../ASTNodes/ASTFormalParamsNode.h"
#include "../../ASTNodes/ASTBlockNode.h"

class infoTuple{
	public:
		//empty constructor
		infoTuple(){};

		//constructor for when we only know the value of the id
		infoTuple(std::string value);

		//constructor for when we only know the type, and we dont know the value
		//used in formalparams mainly
		infoTuple(Token type);

		//constructor for when we know both the id and the token type
		infoTuple(std::string value, Token type);

		//constructor for when we have a function thus we know the
		//block of execution and the formal paramaters node
		infoTuple(ASTFormalParamsNode* parameters, ASTBlockNode* block);

		//destructor
		~infoTuple();

		//needed setters
		void setValue(std::string value);

		//getters
		std::string getValue();
		Token getTokenType();
		ASTFormalParamsNode* getParameters();
		ASTBlockNode* getBlock(); 

	private:
		//holders of values
		std::string value;
		Token type;
		ASTFormalParamsNode* parameters;
		ASTBlockNode* block;
};

#endif