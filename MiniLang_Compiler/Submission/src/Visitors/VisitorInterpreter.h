#ifndef VISITOR_INTERPRETER_H
#define VISITOR_INTERPRETER_H

#include "Visitor.h"
#include "./Interpreter/infoTuple.h"
#include "./Interpreter/interpreterTable.h"
#include <stdexcept>
#include <iostream>
#include <vector>

class VisitorInterpreter:public Visitor{
	public:
		//constructor to perform semantic analysis upon the built ast
		VisitorInterpreter(){};

		//destructor
		~VisitorInterpreter(){};

		//ovverriding visit methods according to astnode type
		void visit(ASTPrintNode* node) override;
		void visit(ASTRealLiteralNode* node)override;
		void visit(ASTReturnNode* node)override;
		void visit(ASTAssignmentNode* node)override;
		void visit(ASTFormalParamNode* node)override;
		void visit(ASTFormalParamsNode* node)override;
		void visit(ASTFuncDeclNode* node)override;
		void visit(ASTFunctionCallNode* node)override;
		void visit(ASTIdentifierNode* node)override;
		void visit(ASTIfNode* node)override;
		void visit(ASTIntegerLiteralNode* node)override;
		void visit(ASTStringLiteralNode* node)override;
		void visit(ASTUnaryNode* node)override;
		void visit(ASTVariableDeclarationNode* node)override;
		void visit(ASTWhileNode* node)override;
		void visit(ASTBinaryExprNode* node)override;
		void visit(ASTBlockNode* node)override;
		void visit(ASTBooleanLiteralNode* node)override;

		//goes through all statement nodes and performs interpretation
		bool interpret(std::vector<ASTStmntNode*> statements);

		//clears scope
		void clearScopeInterpreter();

		//to create and be able to use the interpreter table
		interpreterTable* interTable = new interpreterTable();

		private:

		//will hold the latest value obtained after visiting a binary
		//expression
		infoTuple* lastValue;

		//stack that will hold the params expected of a particular function
		std::stack<std::string> stackOfParams;

		//will hold a boolean to see if the block belongs to a 
		//function or not
		bool functionFlag;

		//will work the value of a binary expression
		void workBinary(infoTuple leftExpression, infoTuple rightExpression, std::string Op);
};

#endif