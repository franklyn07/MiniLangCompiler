//Used by VisitorSemAn.h

#include "./Type.h"
#include<iostream>

//definition of first constructor
Type::Type(std::string myType){
	this -> myType = myType;
}

//definition of second constructor
Type::Type(std::string myType, std::vector<std::string> myParamTypes){
	this -> myType = myType;
	this -> myParamTypes = myParamTypes;
}

//compare the token types
bool Type::compareTokenType(std::string identifierType){
	//if same type return true
	if (myType.compare(identifierType)==0){
		return true;
	}else{
		//else check whether the reqd type is a real and given is an int
		//in that case also accept , else false
		if(myType.compare("Real") == 0 && identifierType.compare("Int")==0){
			return true;
		}
		return false;
	}
}

//comapre the parameters with the expected ones
bool Type::compareParams(std::vector <std::string> parameterTypes){

	//check if they are all of the required type
	for(int i = 0; i < myParamTypes.size();i++){
		std::string paramType = myParamTypes[i];
		std::string currentString = parameterTypes[i];
		//if not same type
		if(paramType.compare(currentString)!=0){
			//check whether the reqd is a real and given is int
			//if not true raise the flag else do nothing
			if(paramType.compare("Real") == 0 && currentString.compare("Int")==0){
				//do not set temp to false and continue to next iteration
				continue;
			}
			std::cout << "Expected: "<<paramType;
			std::cout << " Recieved: "<<currentString<<std::endl;
			return false;
		}
	}

	//reversing back my vector to original order
	std::reverse(myParamTypes.begin(),myParamTypes.end());

	return true;
}