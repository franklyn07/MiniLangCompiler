//Used by VisitorSemAn.h

#ifndef SCOPEDSYMBOLTABLE_H
#define SCOPEDSYMBOLTABLE_H

#include <string>
#include <stack>
#include <unordered_map>
#include "./Type.h"
#include <iostream>

struct Scope{
	std::unordered_map<std::string, Type*> scope;
};

class scopedSymbolTable{
	private:
		//symbolTable holder
		std::stack<Scope> symbolTable;

	public:
		//constructor
		scopedSymbolTable();

		//destructor
		~scopedSymbolTable(){};

		//push new scope on stack
		void pushNewScope(Scope s);

		//pop the top scope from stack
		void popTopScope();

		//insert a type 
		void insertIdandType_CurrentScope(std::string id,Type* t);

		//find item in map 
		//if found return Type else return Null
		Type* findEntity(std::string identifier);

		//search only in current scope
		Type* findInScope(std::string identifier);

		//print top scope of symbol table
		void printTopScope();

		//empties symbol table
		void clearScope();

};

#endif