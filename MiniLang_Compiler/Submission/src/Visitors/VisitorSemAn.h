#ifndef VISITOR_SEMANALYSIS_H
#define VISITOR_SEMANALYSIS_H

#include "Visitor.h"
#include "./SemanticAnalysis/scopedSymbolTable.h"
#include "./SemanticAnalysis/Type.h"
#include <stdexcept>
#include <iostream>
#include <vector>

class VisitorSemAn:public Visitor{
	public:
		//constructor to perform semantic analysis upon the built ast
		VisitorSemAn(){};

		//destructor
		~VisitorSemAn(){};

		//goes through all statement nodes and performs semantic analysis
		bool analyze(std::vector<ASTStmntNode*> statements);

		//clears scope
		void clearScopeAnalyzer();

		//ovverriding visit methods according to astnode type
		void visit(ASTPrintNode* node) override;
		void visit(ASTRealLiteralNode* node)override;
		void visit(ASTReturnNode* node)override;
		void visit(ASTAssignmentNode* node)override;
		void visit(ASTFormalParamNode* node)override;
		void visit(ASTFormalParamsNode* node)override;
		void visit(ASTFuncDeclNode* node)override;
		void visit(ASTFunctionCallNode* node)override;
		void visit(ASTIdentifierNode* node)override;
		void visit(ASTIfNode* node)override;
		void visit(ASTIntegerLiteralNode* node)override;
		void visit(ASTStringLiteralNode* node)override;
		void visit(ASTUnaryNode* node)override;
		void visit(ASTVariableDeclarationNode* node)override;
		void visit(ASTWhileNode* node)override;
		void visit(ASTBinaryExprNode* node)override;
		void visit(ASTBlockNode* node)override;
		void visit(ASTBooleanLiteralNode* node)override;

	
		//to create and be able to use the symbol table
		scopedSymbolTable* sst = new scopedSymbolTable();
	private:

		//will hold the expression type token
		std::string expressionType;

		//will check binary operation
		bool checkBinary(std::string leftExpression, std::string rightExpression, std::string Operator);

		//holds the count of open functions in the scope
		int openFunctions;

		//holds the number of current returns
		int numReturns;

		//will hol current functions parametr types temporarily
		std::vector<std::string> temp;

		//will hold temporarily the types of formal params
		std::unordered_map<std::string, Type*> formalParamTypes;

		//holds the return type of the last function decl
		std::string functionDeclReturnType;

		//creates a copy of a vector
		std::vector<std::string> createCopyVector(std::vector<std::string> myVector);

};

#endif