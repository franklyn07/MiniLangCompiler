#include "ASTIdentifierNode.h"

ASTIdentifierNode::ASTIdentifierNode(std::string identifier){
	this->identifier = identifier;
}

std::string ASTIdentifierNode::getIdentifier(){
	return identifier;
}

/*void ASTBinaryExprNode::accept(Visitor * visitor){
	visitor -> visit(this);
}*/