#include "ASTUnaryNode.h"

ASTUnaryNode::ASTUnaryNode(std::string lexeme, ASTExprNode* expression){
	this->expression = expression;
	this->lexeme = lexeme;
}

ASTUnaryNode::~ASTUnaryNode(){
	delete expression;
}

ASTExprNode* ASTUnaryNode::getExpression(){
	return expression;
}

std::string ASTUnaryNode::getString(){
	return lexeme;
}