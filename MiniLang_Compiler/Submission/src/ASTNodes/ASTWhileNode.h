#ifndef ASTWHILENODE_H
#define ASTWHILENODE_H

#include "ASTStmntNode.h"
#include "ASTExprNode.h"
#include "ASTBlockNode.h"

class ASTWhileNode : public ASTStmntNode{
	public:

		//constructors
		ASTWhileNode(ASTExprNode* expression, ASTBlockNode* block);

		//destructors
		~ASTWhileNode();

		//getters
		ASTExprNode* getExpression();
		ASTBlockNode* getBlock();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};

	private:

		//fields that hold pointers
		ASTExprNode* expression;
		ASTBlockNode* block;

		//to delete
		void dummy() override{};
};

#endif
