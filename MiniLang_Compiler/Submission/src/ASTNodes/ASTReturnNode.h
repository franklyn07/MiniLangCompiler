#ifndef ASTRETURNNODE_H
#define ASTRETURNNODE_H

#include "ASTStmntNode.h"
#include "ASTExprNode.h"

class ASTReturnNode:public ASTStmntNode{
	
	public:

		//constructor
		ASTReturnNode(ASTExprNode* expression);

		//destructor
		~ASTReturnNode();

		//getters
		ASTExprNode* getExpression();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:
		//needs to be destroyed
		void dummy () override{};

		ASTExprNode* expression;
};

#endif