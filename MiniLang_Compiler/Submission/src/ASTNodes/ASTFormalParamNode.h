#ifndef ASTFORMALPARAMNODE_H
#define ASTFORMALPARAMNODE_H

#include "ASTStmntNode.h"
#include "ASTIdentifierNode.h"
#include "../Lexer/Token.h"
#include <string>

class ASTFormalParamNode: public ASTStmntNode{
	public:

		//constructor
		ASTFormalParamNode(ASTIdentifierNode* identifier, std::string tokenType);

		//destructor
		~ASTFormalParamNode(){delete identifier;};

		//getters
		ASTIdentifierNode* getIdentifier();
		std::string getTokenType();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:
		//stores tokentype and identifier
		ASTIdentifierNode* identifier;
		std::string tokenType;

		//to be destroyed
		void dummy() override {};

};

#endif