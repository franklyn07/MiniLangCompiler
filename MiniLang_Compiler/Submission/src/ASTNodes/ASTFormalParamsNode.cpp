#include "ASTFormalParamsNode.h"

void ASTFormalParamsNode::addFormalParam(ASTFormalParamNode* formalParam){
	formalParamNodes.push_back(formalParam);
}

ASTFormalParamsNode::~ASTFormalParamsNode(){
	while(formalParamNodes.size() != 0){
		formalParamNodes.pop_back();
	}
}

std::vector<ASTFormalParamNode*> ASTFormalParamsNode::getVector(){
	return formalParamNodes;
}