#ifndef ASTFUNCDECLNODE_H
#define ASTFUNCDECLNODE_H

#include "ASTStmntNode.h"
#include "ASTBlockNode.h"
#include "ASTIdentifierNode.h"
#include "ASTFormalParamsNode.h"
#include <string>

class ASTFuncDeclNode: public ASTStmntNode{
	public :

		//constructor
		ASTFuncDeclNode(ASTIdentifierNode* identifier, ASTFormalParamsNode* formalParams, std::string tokenType, ASTBlockNode* block);

		//destructor
		~ASTFuncDeclNode();

		//getters
		ASTIdentifierNode* getIdentifier();
		ASTFormalParamsNode* getFormalParams();
		std::string getTokenType();
		ASTBlockNode* getBlock();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//fields holding pointers and details
		ASTIdentifierNode* identifier;
		ASTFormalParamsNode* formalParams; 
		std::string tokenType; 
		ASTBlockNode* block;

		//method to be destroyed
		void dummy() override {};
};

#endif