#ifndef ASTSTRINGLITERALNODE_H
#define ASTSTRINGLITERALNODE_H

#include "./ASTExprNode.h"
#include <string>

class ASTStringLiteralNode: public ASTExprNode{
	
	public:

		//constructor
		ASTStringLiteralNode(std::string stringValue);
		~ASTStringLiteralNode(){};

		std::string getString();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};
		
		std::string stringValue;
};

#endif