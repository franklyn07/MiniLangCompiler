#include "ASTIfNode.h"

ASTIfNode::ASTIfNode(ASTExprNode* expression,ASTBlockNode* blockL){
	this -> expression = expression;
	this -> blockL = blockL;
	this -> blockR = nullptr;
}

ASTIfNode::ASTIfNode(ASTExprNode* expression,ASTBlockNode* blockL, ASTBlockNode* blockR){
	this -> expression = expression;
	this -> blockL = blockL;
	this -> blockR = blockR;
}

ASTIfNode::~ASTIfNode(){
	delete blockL;
	delete blockR;
	delete expression;
}

ASTExprNode* ASTIfNode::getExpression(){
	return expression;
}

ASTBlockNode* ASTIfNode::getLeftBlock(){
	return blockL;
}

ASTBlockNode* ASTIfNode::getRightBlock(){
	return blockR;
}