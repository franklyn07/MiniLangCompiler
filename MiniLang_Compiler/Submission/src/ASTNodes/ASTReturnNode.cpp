#include "ASTReturnNode.h"

ASTReturnNode::ASTReturnNode (ASTExprNode* expression){
	this -> expression = expression;
}

ASTReturnNode::~ASTReturnNode(){
	delete expression;
}

ASTExprNode* ASTReturnNode::getExpression(){
	return expression;
}