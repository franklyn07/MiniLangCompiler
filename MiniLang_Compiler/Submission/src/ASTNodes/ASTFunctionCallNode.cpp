#include "ASTFunctionCallNode.h"

ASTFunctionCallNode::ASTFunctionCallNode(ASTIdentifierNode* identifier){
	this->identifier = identifier;
	firstExpr= nullptr;
	secondExpr = nullptr;
}

ASTFunctionCallNode::ASTFunctionCallNode(ASTIdentifierNode* identifier, ASTExprNode* firstExpr){
	this->identifier = identifier;
	this->firstExpr= firstExpr;
	secondExpr = nullptr;
}

ASTFunctionCallNode::ASTFunctionCallNode(ASTIdentifierNode* identifier, ASTExprNode* firstExpr, ASTExprNode* secondExpr){
	this->identifier = identifier;
	this->firstExpr= firstExpr;
	this->secondExpr = secondExpr;
}

ASTFunctionCallNode::~ASTFunctionCallNode(){
	delete identifier;
	delete firstExpr;
	delete secondExpr;
}

ASTIdentifierNode* ASTFunctionCallNode::getIdentifier(){
	return identifier;
}

ASTExprNode* ASTFunctionCallNode::getFirstExprNode(){
	return firstExpr;
}

ASTExprNode* ASTFunctionCallNode::getSecondExprNode(){
	return secondExpr;
}