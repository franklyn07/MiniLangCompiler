#ifndef ASTPRINTNODE_H
#define ASTPRINTNODE_H

#include "ASTStmntNode.h"
#include "ASTExprNode.h"

class ASTPrintNode:public ASTStmntNode{
	
	public:

		//constructor
		ASTPrintNode(ASTExprNode* expression);

		//destructor
		~ASTPrintNode();

		//getters
		ASTExprNode * getExpression();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed
		void dummy () override{};

		ASTExprNode* expression;
};

#endif