#ifndef ASTREALLITERALNODE_H
#define ASTREALLITERALNODE_H

#include "./ASTExprNode.h"

class ASTRealLiteralNode: public ASTExprNode{
	
	public:

		//constructor
		ASTRealLiteralNode(float realValue);
		~ASTRealLiteralNode(){};

		//getter
		float getRealValue();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};

		float realValue;
};

#endif