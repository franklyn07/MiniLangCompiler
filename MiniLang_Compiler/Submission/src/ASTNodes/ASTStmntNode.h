#ifndef ASTSTMNTNODE_H
#define ASTSTMNTNODE_H

#include "./ASTNode.h"

class ASTStmntNode: public ASTNode{
	public:
	
		//virtual destructor to enable overriding
		virtual ~ASTStmntNode(){};

	private:
		//dummy method is inherited to make it a pure virtual class
};

#endif