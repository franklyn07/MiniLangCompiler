#ifndef ASTVARIABLEDECLARATIONNODE_H
#define ASTVARIABLEDECLARATIONNODE_H

#include "ASTStmntNode.h"
#include "ASTExprNode.h"
#include "ASTIdentifierNode.h"
#include "../Lexer/Token.h"

class ASTVariableDeclarationNode: public ASTStmntNode{
	public:

		//constructor
		ASTVariableDeclarationNode(ASTIdentifierNode* identifier, std::string type, ASTExprNode* exprNodePointer);

		//destructor
		~ASTVariableDeclarationNode();

		//getters
		ASTIdentifierNode* getIdentifier();
		std::string getTokenType();
		ASTExprNode* getExpression();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};

	private:

		//needs to be destroyed

		void dummy()override{};

		ASTIdentifierNode* identifier;
		std::string type;
		ASTExprNode* exprNodePointer;
};

#endif