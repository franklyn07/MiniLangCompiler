#ifndef ASTUNARYNODE_H
#define ASTUNARYNODE_H

#include "./ASTExprNode.h"
#include <string>

class ASTUnaryNode:public ASTExprNode{
	public:

		//constructor
		ASTUnaryNode(std::string lexeme, ASTExprNode* expression);

		//destructor
		~ASTUnaryNode();

		//getters
		ASTExprNode* getExpression();
		std::string getString();

		//overriding accept visitor method in order for the ast nodes 
		void accept(Visitor* v) override {v->visit(this);};


	private:

		//needs to be destroyed - implemented to make class non virtual
		void dummy()override{};

		//will hold pointer to parsed expression
		ASTExprNode* expression;

		//will store lexeme of unary
		std::string lexeme;
};

#endif