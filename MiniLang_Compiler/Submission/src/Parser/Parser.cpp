#include "Parser.h"

using namespace std;

//will take a pointer to the lexer instance to be able to use its methods
Parser::Parser(Lexer* lexer){
	this->lexer = lexer;
}

//will use the instance of lexer to invoke its methods and call its tokens
//in order to build the parsing/abstract syntax tree
bool Parser::parse(){
	/*in bnf we find that a program is made up of a set of statements
	thus we will be parsing those statements untill we find the token eof.
	With each step we will be keeping a pointer to the top most node of the
	particular statement, as denoted in the AST. */

	while(lexer->previewNextToken().tokenType != Token::Tok_EOF){
		//if any error occurs whislt parsing the program, ti should be
		//caught here, and user notified of error.
		try{
			astStatementNodes.push_back(parseStatement());
		}catch(const char * errorMess){
			cout << errorMess << endl;
			return false; 
		}
	}

	cout<<"AST Built!"<<endl;
	return true;
}

//parses a statement
ASTStmntNode* Parser::parseStatement(){
	//pointer to returning node - will contain parent leaf of statement
	ASTStmntNode * node;

	//get current token - you are safe to assume it is not eof due to check in parse() 
	token currentToken = lexer->getCurrentToken();
	switch (currentToken.tokenType){
		case Token::Tok_KW_var:
			//parse the var statement and return the pointer which points
			//to the produced child tree
			node = parseVariableDeclarationStatement();

			break;		
		case Token::Tok_KW_set:
			//parse the set statement and return the pointer which points
			//to the produced child tree
			node = parseAssignmentStatement();
			break;
		case Token::Tok_KW_print:
			//parse the print statement and return the pointer which points
			//to the produced child tree
			node = parsePrintStatement();
			break;
		case Token::Tok_KW_if:
			//parse the if statement and return the pointer which points
			//to the produced child tree
			node = parseIfStatement();
			break;
		case Token::Tok_KW_while:
			//parse the while statement and return the pointer which points
			//to the produced child tree
			node = parseWhileStatement();
			break;
		case Token::Tok_KW_return:
			//parse the return statement and return the pointer which points
			//to the produced child tree
			node = parseReturnStatement();
			break;
		case Token::Tok_KW_def:
			//parse the def statement and return the pointer which points
			//to the produced child tree
			node = parseFuncDeclStatement();
			break;
		case Token::Tok_Punctuation:
			//parse the block statement and return the pointer which points
			//to the produced child tree
			//check if block starts with a '{'
			if(checkPunct(currentToken,"{")){
				node = parseBlockStatement();
				break;
			}else{
				node = nullptr;
				throw "Syntax Error! Illegal start of a block!";
				break;
			}
		case Token::Tok_ERROR:
			//if token is an error token print out unexpected token
			node = nullptr;
			throw "Syntax Error! Error Occured!";
			break; 
		default:
			//if it doesn't match anything return nullptr - invalid statement
			throw "Syntax Error! Statement Declaration Should Commence With A Standard Keyword!";
			node = nullptr;
			break;
	}

	return node;
}

//EXPRESSION AND ITS DERIVATIONS PARSING

//parses an expression
ASTExprNode* Parser::parseExpression(){
	//will hold the relational operator lexeme if any
	string relOpLexeme;

	//parse simple expression and store pointer to its node
	ASTExprNode * simpleExpressionL = parseSimpleExpression();
	
	//check next token without getting it
	token currentToken = lexer->previewNextToken(); 

	//if next token is relop get the token and parse the next simpleexpression
	if(currentToken.tokenType == Token::Tok_RelOp){
		currentToken = lexer -> getCurrentToken();
		relOpLexeme = currentToken.lexeme;
		ASTExprNode * simpleExpressionR = parseSimpleExpression();
		return new ASTBinaryExprNode(relOpLexeme, simpleExpressionL, simpleExpressionR);
	}else{
		//else just return a pointer to the first parsed simple expression
		return simpleExpressionL;
	}
}

//parses simple expression
ASTExprNode* Parser::parseSimpleExpression(){
	//will hold the relational operator lexeme if any
	string addOpLexeme;

	//parse term and store pointer to its node
	ASTExprNode * termL = parseTerm();

	//check next token without getting it
	token currentToken = lexer->previewNextToken();

	//if next token is addop get token and parse the next term
	if(currentToken.tokenType == Token::Tok_AddOp){
		currentToken = lexer -> getCurrentToken();
		addOpLexeme = currentToken.lexeme;
		ASTExprNode* termR = parseTerm();
		return new ASTBinaryExprNode(addOpLexeme,termL,termR);
	}else{
		//else just return pointer to the first parsed term
		return termL;
	}
}

//parses term
ASTExprNode* Parser::parseTerm(){
	//will hold the multiplicative operator lexeme if any
	string multOpLexeme;

	//parse factor and store pointer to its node
	ASTExprNode * factorL = parseFactor();

	//check next token without getting it
	token currentToken = lexer->previewNextToken();

	//if next token is multop get token and parse the next term
	if(currentToken.tokenType == Token::Tok_MultOp){
		currentToken = lexer -> getCurrentToken();
		multOpLexeme = currentToken.lexeme;
		ASTExprNode* factorR = parseFactor();
		return new ASTBinaryExprNode(multOpLexeme,factorL,factorR);
	}else{
		//else just return pointer to the first parsed factor
		return factorL;
	}
}

//parses factor
ASTExprNode* Parser::parseFactor(){
	//get token
	token currentToken = lexer -> getCurrentToken();

	//check if its a literal
	int checkLiteralValue = checkLiteral(currentToken);


	
	if(checkLiteralValue != -1 ){
		return getLiteralNode(checkLiteralValue, currentToken.lexeme);
	}else if (currentToken.tokenType == Token::Tok_Identifier){
		//since both function call and identifier start with an identifier
		//check next token for a '(', to see whether its a functioncall
		token tempToken = lexer -> previewNextToken();
		if(tempToken.tokenType == Token::Tok_Punctuation && tempToken.lexeme.compare("(")==0){
			//get the current token
			lexer ->getCurrentToken();

			//store the function call identifier
			string identifier = currentToken.lexeme;

			//parse the functionCall and get a pointer to it
			ASTExprNode* functionCall = parseFunctionCall(identifier);

			//check if function call is closed by a ')'
			currentToken = lexer -> previewNextToken();

			if (currentToken.tokenType != Token::Tok_Punctuation){
				throw "Syntax Error! ')' Expected When Closing A Function Call!";
				return nullptr;
			}else{
				if(currentToken.lexeme.compare(")")!=0){
					throw "Syntax Error! ')' Expected When Closing A Function Call!";
					return nullptr;
				}
			}

			//removing the ')'
			lexer -> getCurrentToken();

			//if funcioncall is properly formulated, return the parsed expression pointer
			return functionCall;
		}else{
			//if its only an identifier
			ASTExprNode* identifier = new ASTIdentifierNode(currentToken.lexeme);
			return identifier;
		}
	}else if(currentToken.tokenType == Token::Tok_Punctuation && currentToken.lexeme.compare("(")==0){
		ASTExprNode* expression = parseExpression();

		//check if sub expression is closed by a ')'
		currentToken = lexer -> previewNextToken();

		if (currentToken.tokenType != Token::Tok_Punctuation){
			throw "Syntax Error! ')' Expected When Closing A Function Call!";
			return nullptr;
		}else{
			if(currentToken.lexeme.compare(")")!=0){
				throw "Syntax Error! ')' Expected When Closing A Function Call!";
				return nullptr;
			}
		}

		//removing the ')'
		lexer -> getCurrentToken();

		//if subexpression is properly formulated, return the parsed expression pointer
		return expression;
	}else if (currentToken.tokenType == Token::Tok_KW_not || (currentToken.tokenType == Token::Tok_AddOp && currentToken.lexeme.compare("-")==0)){
		// if unary return a unary node
		ASTExprNode* unaryNode = new ASTUnaryNode(currentToken.lexeme,parseExpression());
		return unaryNode;
	}else{
		throw "Syntax Error! Invalid Statement Encountered Whilst Parsing!";
		return nullptr;
	}
}

//parses function call
ASTExprNode* Parser::parseFunctionCall(string identifier){
	
	//check if function call has no arguments
	if((lexer -> previewNextToken()).lexeme.compare(")")==0){
		return new ASTFunctionCallNode(new ASTIdentifierNode(identifier));
	}else{
		//store first argument
		ASTExprNode* functionArgumentsL = parseExpression();

		//check if next token is a ','
		if((lexer ->previewNextToken()).lexeme.compare(",")==0){
			//remove ','
			lexer -> getCurrentToken();
			//parse next expression argumetn and store a pointer to it
			ASTExprNode* functionArgumentsR = parseExpression();
			return new ASTFunctionCallNode(new ASTIdentifierNode(identifier), functionArgumentsL, functionArgumentsR);
		}

		//if only one argument return that one only
		return new ASTFunctionCallNode(new ASTIdentifierNode(identifier), functionArgumentsL);
	}

	
}

//getter for vector of pointers to statement nodes which make up the ast
vector<ASTStmntNode*> Parser::getASTStatementNodes(){
	return astStatementNodes;
}

//cleaner for aststatement nodes vector
void Parser::clearASTStatementNodes(){
	while(!astStatementNodes.empty()){
		delete astStatementNodes.back();
		astStatementNodes.pop_back();
	}
}

//STATEMENT PARSING

//parses a variable declaration statement
ASTVariableDeclarationNode* Parser::parseVariableDeclarationStatement(){
	//stores the identifier
	ASTIdentifierNode* identifier;

	//stores the type of the identifier
	string tokenType;

	//get current token in line
	token currentToken = lexer->previewNextToken();

	
	//check if next token is an identifier
	if(currentToken.tokenType != Token::Tok_Identifier){
		throw "Syntax Error! Identifier Expected In Variable Declaration Statement.";
		//if error is encountered stop parsing statement and return nullptr
		return nullptr;
	}

	//get rid of token
	lexer -> getCurrentToken();

	//if no error encountered assign tokens lexeme to the node 
	identifier = new ASTIdentifierNode(currentToken.lexeme);

	//see next token
	currentToken = lexer->previewNextToken();

	//Check if variable declaration has ":"
	if (!checkPunct(currentToken,":")){
		return nullptr;
	}

	//get rid of token if correct
	lexer->getCurrentToken();

	//check next Token
	currentToken = lexer->previewNextToken();

	//check if token is of type Type
	if(!checkType(currentToken)){
		throw "Syntax Error! Type Expected In Variable Declaration.";
		return nullptr;
	}

	//get rid of token if correct
	lexer -> getCurrentToken();

	//store token type if a valid type
	tokenType = d.printTokenType(currentToken.tokenType);

	//check next token
	currentToken = lexer->previewNextToken();

	//check if there is '='
	if(currentToken.tokenType != Token::Tok_AssignmentOp){
		throw "Syntax Error! '=' Expected In Variable Declaration!";
		return nullptr;
	}

	//get rid of token if correct
	lexer->getCurrentToken();

	//create a pointer to the parent node of the expression mini parsing tree
	ASTExprNode* expressionNode = parseExpression();

	//check next token
	currentToken = lexer->previewNextToken();

	//check if variable declaration ends with a ";"
	if(!checkPunct(currentToken,";")){
		return nullptr;
	}

	//if ';' get it and throw it away
	lexer -> getCurrentToken();

	return new ASTVariableDeclarationNode(identifier,tokenType,expressionNode);
}

//parses an assignment statement
ASTAssignmentNode* Parser::parseAssignmentStatement(){
	//stores the identifier
	ASTIdentifierNode* identifier;

	//stores pointer to expression node
	ASTExprNode* expression;

	//stores the current token
	token currentToken = lexer -> previewNextToken();

	//check if next token is an identifier
	if(currentToken.tokenType != Token::Tok_Identifier){
		throw "Syntax Error! Identifier Expected In Assignment Statement!";
		//if error is encountered stop parsing statement and return nullptr
		return nullptr;
	}

	//if correct get it
	lexer -> getCurrentToken();

	//assign identifier
	identifier = new ASTIdentifierNode(currentToken.lexeme);

	//check token
	currentToken = lexer ->previewNextToken();

	//check if there is '='
	if(currentToken.tokenType != Token::Tok_AssignmentOp){
		throw "Syntax Error! '=' Expected In Assignment Statement!";
		return nullptr;
	}

	//if correct get it
	lexer -> getCurrentToken();

	//parse expression
	expression = parseExpression();

	//check next token
	currentToken = lexer->previewNextToken();

	//check if variable declaration ends with a ";"
	if(!checkPunct(currentToken,";")){
		return nullptr;
	}

	//if ';' get it and throw it away
	lexer -> getCurrentToken();

	return new ASTAssignmentNode(identifier,expression);

}

//parses a print statement
ASTPrintNode* Parser::parsePrintStatement(){
	ASTExprNode* expression = parseExpression();

	//check next token
	token currentToken = lexer->previewNextToken();

	//check if variable declaration ends with a ";"
	if(!checkPunct(currentToken,";")){
		return nullptr;
	}

	//if ';' get it and throw it away
	lexer -> getCurrentToken();

	return new ASTPrintNode(expression);
}


//parses a return statement
ASTReturnNode* Parser::parseReturnStatement(){
	ASTExprNode* expression = parseExpression();

	//check next token
	token currentToken = lexer->previewNextToken();

	//check if variable declaration ends with a ";"
	if(!checkPunct(currentToken,";")){
		return nullptr;
	}

	//if ';' get it and throw it away
	lexer -> getCurrentToken();

	return new ASTReturnNode(expression);	
}


//parses a block statement
ASTBlockNode* Parser::parseBlockStatement(){
	//create new block
	ASTBlockNode* block = new ASTBlockNode();
	token currentToken = lexer -> previewNextToken();

	do{
		//check if we reached eof
		if (currentToken.tokenType == Token:: Tok_EOF){
			throw "Reached EOF While Parsing! '}' Missing!";
			return nullptr;
		}

		block -> addStatement(parseStatement());

		currentToken = lexer -> previewNextToken();

	}while(currentToken.lexeme.compare("}")!=0);


	//if '}' is found get rid of it and return the block pointer
	lexer->getCurrentToken();
	return block;
}

//parses an if statement
ASTIfNode* Parser::parseIfStatement(){
	//pointer to if node
	ASTIfNode* ifNode;

	//check next token
	token currentToken = lexer->previewNextToken();

	if (!checkPunct(currentToken,"(")){
		return nullptr;
	}

	//if '(' get rid of it
	lexer->getCurrentToken();

	ASTExprNode* expression = parseExpression();

	currentToken = lexer->previewNextToken();

	if (!checkPunct(currentToken,")")){
		return nullptr;
	}

	//get rid of ")"
	lexer->getCurrentToken();

	//check for "{" when starting block
	currentToken = lexer->previewNextToken();
	
	if (!checkPunct(currentToken,"{")){
			return nullptr;
		}

	//if '{' get rid of it
	lexer->getCurrentToken();


	ASTBlockNode* blockL = parseBlockStatement();

	//check if there is an else statement
	currentToken = lexer->previewNextToken();
	if(currentToken.tokenType == Token::Tok_KW_else){
		//get rid of else
		lexer->getCurrentToken();

		//check for "{" when starting block
		currentToken = lexer->previewNextToken();
	
		if (!checkPunct(currentToken,"{")){
			return nullptr;
		}

		//if '{' get rid of it
		lexer->getCurrentToken();

		//parse right block
		ASTBlockNode* blockR = parseBlockStatement();

		//parse else block statement
		ifNode = new ASTIfNode(expression,blockL, blockR);
	}else{
		//else return parsed expression and its block
		ifNode = new ASTIfNode(expression,blockL);
	}

	return ifNode;
}

//parses while statements
ASTWhileNode* Parser::parseWhileStatement(){
	token currentToken = lexer->previewNextToken();

	if (!checkPunct(currentToken,"(")){
		return nullptr;
	}

	//if '(' get rid of it
	lexer->getCurrentToken();

	//parse expression
	ASTExprNode* expression = parseExpression();

	currentToken = lexer->previewNextToken();

	if (!checkPunct(currentToken,")")){
		return nullptr;
	}

	//if ')' get rid of it
	lexer->getCurrentToken();

	currentToken = lexer->previewNextToken();

	//blocks start with '{' which need to be removed
	if (!checkPunct(currentToken,"{")){
		return nullptr;
	}

	//if '{' get rid of it
	lexer->getCurrentToken();

	ASTBlockNode* block = parseBlockStatement();

	return new ASTWhileNode(expression, block);
}

//parses function declaration statements
ASTFuncDeclNode* Parser::parseFuncDeclStatement(){
	//will hold identifier
	ASTIdentifierNode* identifier;

	//will hold formal params node
	ASTFormalParamsNode* formalParams;

	//will told token type
	string tokenType;

	//will hold block 
	ASTBlockNode* block;

	//get identifier
	token currentToken = lexer -> previewNextToken();

	if(currentToken.tokenType != Token::Tok_Identifier){
		throw "Syntax Error! Identifier Expected!";
		return nullptr;
	}

	//if identifier store the identifier and throw the token
	identifier = new ASTIdentifierNode(currentToken.lexeme);

	lexer ->getCurrentToken();

	//check for '('
	currentToken = lexer -> previewNextToken();

	if(!checkPunct(currentToken,"(")){
		return nullptr;
	}

	//else get rid of it
	lexer->getCurrentToken();

	//parse formal parameters
	formalParams = parseFormalParams();

	//check for ')'
	currentToken = lexer -> previewNextToken();

	if(!checkPunct(currentToken,")")){
		return nullptr;
	}

	//else get rid of it
	lexer->getCurrentToken();

	//check for ':'
	currentToken = lexer -> previewNextToken();

	if(!checkPunct(currentToken,":")){
		return nullptr;
	}

	//else get rid of it
	lexer->getCurrentToken();

	currentToken = lexer ->previewNextToken();

	//check type of current token
	if(!checkType(currentToken)){
		throw "Syntax Error! Type Expected In Function Declaration!";
		return nullptr;
	}

	//else store it get rid of token
	tokenType = d.printTokenType(currentToken.tokenType);

	lexer->getCurrentToken();

	//check for '{' of block
	currentToken = lexer -> previewNextToken();

	if(!checkPunct(currentToken,"{")){
		return nullptr;
	}

	//get rid of it
	lexer -> getCurrentToken();

	block = parseBlockStatement();

	return new ASTFuncDeclNode(identifier, formalParams, tokenType, block);
}

//parses formal params
ASTFormalParamsNode* Parser::parseFormalParams(){
	//will hold pointer to formalParams node
	ASTFormalParamsNode* formalParams = new ASTFormalParamsNode();

	//pointer to parsed formal param
	ASTFormalParamNode* formalParam = parseFormalParam();

	//add parsed formal param to formal params
	formalParams -> addFormalParam(formalParam);

	//check if there are more formal params
	token currentToken = lexer -> previewNextToken();

	//while there are more formal params keep parsing them
	while(currentToken.lexeme.compare(",")==0){
		//get rid of ','
		lexer -> getCurrentToken();
		//parse next formal param and add it to formal params
		formalParam = parseFormalParam();
		formalParams -> addFormalParam(formalParam);
		//check what there is after current formal param
		currentToken = lexer -> previewNextToken();
	}

	return formalParams;
}

//parses formal param
ASTFormalParamNode* Parser::parseFormalParam(){
	//will hold identifier
	ASTIdentifierNode* identifier;

	//will hold token type
	string tokenType;

	//check next token
	token currentToken = lexer -> previewNextToken();

	//if identifier
	if(currentToken.tokenType != Token::Tok_Identifier){
		throw "Syntax Error! Identifier Expected As A Formal Parameter!";
		return nullptr;
	}

	//if yes store it and get rid of token
	identifier = new ASTIdentifierNode(currentToken.lexeme);
	lexer->getCurrentToken();

	//check for ':'
	currentToken = lexer -> previewNextToken();

	if(!checkPunct(currentToken,":")){
		return nullptr;
	}
	
	//get rid of token
	lexer -> getCurrentToken();

	//check for token type
	currentToken = lexer->previewNextToken();

	if(!checkType(currentToken)){
		throw "Syntax Error! Token Type Expected As A Parameter!";
		return nullptr;
	}

	//else store it and get rid of it
	tokenType = d.printTokenType(currentToken.tokenType);
	lexer->getCurrentToken();

	return new ASTFormalParamNode(identifier,tokenType);
}


//MISC FUNCTIONS

//checks if token is of the type : Type
bool Parser::checkType(token t){
	switch(t.tokenType){
		case Token::Tok_KW_real:
		case Token::Tok_KW_int:
		case Token::Tok_KW_bool:
		case Token::Tok_KW_string:
			return true;
		default:
			return false;
	}
}

//checks if token is of type : Literal
int Parser::checkLiteral(token t){
	switch(t.tokenType){
		case Token::Tok_TypeInt:
			return 0;
		case Token::Tok_TypeReal:
			return 1;
		case Token::Tok_TypeBool:
			return 2;
		case Token::Tok_TypeString:
			return 3;
		default:
			return -1;
	}
}

//returns a node of the appropriate literal type
ASTExprNode* Parser::getLiteralNode(int literalVal, string lexeme){
	ASTExprNode* literalNode;
	switch(literalVal){
		case 0:
			literalNode = new ASTIntegerLiteralNode(std::stoi(lexeme));
			break;
		case 1:
			literalNode = new ASTRealLiteralNode(std::stof(lexeme));
			break;
		case 2:
			if (lexeme.compare("true")==0){
				literalNode = new ASTBooleanLiteralNode(true);
			}else{
				literalNode = new ASTBooleanLiteralNode(false);
			}
			break;
		case 3:
			literalNode = new ASTStringLiteralNode(lexeme);
			break;
		default:
			literalNode = nullptr;
	}
	return literalNode;
}

//checks if needed punctuation is present
bool Parser::checkPunct(token t, string s){
	if (t.tokenType != Token::Tok_Punctuation){
		throw "Syntax Error! Punctuation Missing In Statement.";
		//if error return null
		return false;
	}else{
		if(t.lexeme.compare(s)!=0){
			throw "Syntax Error! Different Punctuation Expected In Statement.";
			//if error return null
			return false;
		}	
	}

	return true;
}