#ifndef PARSER_H
#define PARSER_H

#include "../Lexer/Lexer.h"
#include "../ASTNodes/ASTNode.h"
#include "../ASTNodes/ASTStmntNode.h"
#include "../ASTNodes/ASTExprNode.h"
#include "../ASTNodes/ASTVariableDeclarationNode.h"
#include "../ASTNodes/ASTAssignmentNode.h"
#include "../ASTNodes/ASTPrintNode.h"
#include "../ASTNodes/ASTIfNode.h"
#include "../ASTNodes/ASTWhileNode.h"
#include "../ASTNodes/ASTReturnNode.h"
#include "../ASTNodes/ASTFuncDeclNode.h"
#include "../ASTNodes/ASTBlockNode.h"
#include "../ASTNodes/ASTBinaryExprNode.h"
#include "../ASTNodes/ASTBooleanLiteralNode.h"
#include "../ASTNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ASTIdentifierNode.h"
#include "../ASTNodes/ASTRealLiteralNode.h"
#include "../ASTNodes/ASTIntegerLiteralNode.h"
#include "../ASTNodes/ASTStringLiteralNode.h"
#include "../ASTNodes/ASTUnaryNode.h"
#include "../ASTNodes/ASTFormalParamNode.h"
#include "../ASTNodes/ASTFormalParamsNode.h"
#include "../Lexer/Debugging.h"
#include <iostream>

class Parser{
	private:

	//will hold instance of the lexer
	Lexer * lexer;

	//will hold the produced astStatementNodes which make up the AST
	vector<ASTStmntNode*> astStatementNodes;


	//will parse the current statement and return a pointer to
	//the parent node of such statement
	ASTStmntNode* parseStatement();

	//will parse the current expression and return a pointer to
	//the parent node of such expression
	ASTExprNode* parseExpression();

	//all the different methods which are parsed through an expression
	ASTExprNode* parseSimpleExpression();
	ASTExprNode* parseTerm();
	ASTExprNode* parseFactor();
	ASTExprNode* parseFunctionCall(string identifier);

	//all the different methods which parse the different kind of statements
	ASTVariableDeclarationNode* parseVariableDeclarationStatement();
	ASTAssignmentNode* parseAssignmentStatement();
	ASTPrintNode* parsePrintStatement();
	ASTIfNode* parseIfStatement();
	ASTWhileNode* parseWhileStatement();
	ASTReturnNode* parseReturnStatement();
	ASTFuncDeclNode* parseFuncDeclStatement();
	ASTBlockNode* parseBlockStatement();
	ASTFormalParamsNode* parseFormalParams();
	ASTFormalParamNode* parseFormalParam();

	//checks if token is of type TYPE
	bool checkType(token t);

	//checks if needed punctuation is found
	bool checkPunct(token t, string s);

	//check if token is of type literal
	int checkLiteral(token t);

	//creates a literal node depending on the token type
	ASTExprNode* getLiteralNode(int literalVal, string lexeme);

	//Debugging purposes
	Debugging d;

	public:

	Parser(Lexer* lexer);
	~Parser(){};

	//returns the ASTStatementNode vector
	vector<ASTStmntNode*> getASTStatementNodes();

	//clears ASTStatementNode vector
	void clearASTStatementNodes();

	//will parse the tokens and build the parsing tree
	bool parse();
};

#endif

