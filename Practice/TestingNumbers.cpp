#include <string>
#include <stack> 
#include <stdio.h>

using namespace std;

stack<int> stateStack;
int state = 0;
string lexeme = "";

void tester(string toTest);
void stackClear(stack <int> myStack);
bool isAccept(int State);
int charType(char inputChar);
int transFunc(int state, int charType);

int main(){
	stateStack.push(-2);

	string test1 = "455", test2 = "75.32";

	tester(test1);
	stackClear(stateStack);
	tester(test2);

}

void tester(string toTest){
	int counter = 0;

	while(state != -1){
		char currentChar = (char) toTest[counter];
		lexeme += currentChar;
		if(isAccept(state))
			stackClear(stateStack);
		stateStack.push(state);
		char cat = charType(currentChar);
		state = transFunc(state, cat);
		counter +=1;
	}

	while(isAccept(state)!=true && state != -2){
		state = stateStack.top();
		stateStack.pop();
		lexeme[lexeme.length() - 1] = '\n';
	}

	printf("%s\n", lexeme.c_str());
	
	if(isAccept(state) == true){
		if (state == 1){
			printf("Integer\n");
		}else if (state == 2){
			printf("Float\n");
		}else{		
			printf("Error\n");
		}
	}else{
		printf("Invalid\n");
	}
}


//checks whether state is an accept state
bool isAccept(int State){
	if (State == 1 || State == 2){
		return true;
	}else{
		return false;
	}
}

//return input character type
int charType(char inputChar){
	switch(inputChar){
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return 0;
		case '.':
			return 1;
		default:
			return -1;
	}
}

//performs transition from one state to another
int transFunc(int state, int charType){
	int transitions [4] [2] = {{1 ,-1},
							{1,3,},
							{2,-1},
							{2,-1}};

	if(state == -1 || charType == -1){
		return -1;
	}else{
		return transitions[state][charType];
	}
}

void stackClear(stack <int> myStack){
	int myInt = 0;
	do{
		myInt = myStack.top();
		myStack.pop();
	}while(myInt != -2);

	myStack.push(-2);
}